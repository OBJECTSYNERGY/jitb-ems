﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OS.EMS.Web.Models
{
    public class WorkedAt
    {
        
        public string CompanyName { get; set; }
        public string From { get; set; }
        public int To { get; set; }

    }
}