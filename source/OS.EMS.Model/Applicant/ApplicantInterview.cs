﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OS.EMS.Model
{
    public class ApplicantInterview : Entity
    {
        public Guid ApplicantId { get; set; }

        [ForeignKey("ApplicantId")]
        public virtual Applicant Applicant { get; set; }

        [StringLength(64)]
        public string IntervieSubject { get; set; }

        [StringLength(32)]
        public string InterviewerGuideName1 { get; set; }

        [StringLength(32)]
        public string InterviewerGuideName2 { get; set; }

        [StringLength(64)]
        public string InterviewResult { get; set; }

        [StringLength(256)]
        public string Comments { get; set; }
        public DateTime? InterviewDate { get; set; }

        
    }
}