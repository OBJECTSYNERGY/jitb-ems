﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OS.EMS.Model
{
    public class SystemUser : AuditEntity
    {
        public Guid? RoleId { get; set; }
        public virtual Role Role { get; set; }
        
        public Guid? EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [StringLength(32)]
        public string FirstName { get; set; }

        [StringLength(32)]
        public string LastName { get; set; }
        //public string Phone { get; set; }

        [StringLength(64)]
        public string Email { get; set; }

        [StringLength(32)]
        public string Password { get; set; }

    }
}


 
