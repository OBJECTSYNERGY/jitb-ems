﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    public interface IApplicantEducationService : IBusinessService
    {
        bool Create(ApplicantEducation applicantEducation);
        bool Update(ApplicantEducation applicantEducation);
        IEnumerable<ApplicantEducation> GetAll();
        ApplicantEducation Get(Guid id);
        
    }

    public class ApplicantEducationService : IApplicantEducationService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        public ApplicantEducationService(IAppUow uow)
        {
            Uow = uow;
        }

        public bool Create(ApplicantEducation applicantEducation)
        {
            try
            {
                Uow.ApplicantEducations.Add(applicantEducation);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create ApplicantEducation");
                return false;
            }            
        }

        public bool Update(ApplicantEducation applicantEducation)
        {
            try
            {
                Uow.ApplicantEducations.Update(applicantEducation);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update ApplicantEducation");
                return false;
            }     
        }

        public IEnumerable<ApplicantEducation> GetAll()
        {
            return Uow.ApplicantEducations.GetAll();
        }

        public ApplicantEducation Get(Guid Id)
        {
            return Uow.ApplicantEducations.GetById(Id);
        }

       

    } 
}
