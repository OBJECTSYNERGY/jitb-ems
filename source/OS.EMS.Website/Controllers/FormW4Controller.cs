﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Services;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{

    public class FormW4Controller : BaseController
    {

        [Inject]
        public FormW4Controller(IFormW4Service formW4Service, IEmployeeService employeeService, IApplicantService applicantService, IStoreService storeServices, IAttendanceService attendanceService, IGeographyService geographyService, IAcceptableDocumentServices acceptableDocumentService)
        {
            ApplicantService = applicantService;
            EmployeeService = employeeService;
            AttendanceService = attendanceService;
            StoreService = storeServices;
            GeographyService = geographyService;
            FormW4Service = formW4Service;
            AcceptableDocumentService = acceptableDocumentService;

        }

        #region CRUD Operations

        //FOR FormW4
        public ActionResult Create(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var model = FormW4Service.GetByEmployeeId(id.Value);
            var viewModel = model.ToViewModel();

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Create(FormW4VM viewModel, FormCollection frmColl)
        {
            if (viewModel == null) return RedirectToAction("InvalidRequest", "Error");

            var FormW4 = viewModel.ToModel();

            if (FormW4Service.Create(FormW4))
            {
                ShowMessage(Messages.CreateFormW7Success, MessageType.Success);
                return RedirectToAction("Details", "Employee", new { id = FormW4.EmployeeId });
            }

            return RedirectToAction("InvalidRequest", "Error");
        }

        public ActionResult Update(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var model = FormW4Service.Get(id.Value);
            var viewModel = model.ToViewModel();
            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Update(FormW4VM viewModel)
        {
            if (viewModel == null || viewModel.Id == null) return RedirectToAction("InvalidRequest", "Error");

            var model = viewModel.ToModel();
            if (FormW4Service.Update(model))
            {
                ShowMessage(Messages.UpdateFormW7Success, MessageType.Success);
                return RedirectToAction("Details", "Employee", new { id = model.EmployeeId });
            }

            return RedirectToAction("InvalidRequest", "Error");
        }

        #endregion

        #region HelperMethods
        #endregion

    }

}
