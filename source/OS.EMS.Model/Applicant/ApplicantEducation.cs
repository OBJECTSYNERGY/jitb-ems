using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OS.EMS.Model
{
    public class ApplicantEducation : Entity
    {
        public Guid ApplicantId { get; set; }

        [ForeignKey("ApplicantId")]
        public virtual Applicant Applicant { get; set; }

        public Guid EducationTypeId { get; set; }

        //[System.ComponentModel.DataAnnotations.ScaffoldColumn(true)]
        //public virtual EducationType Type { get; set; }
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(128)]
        public string Location { get; set; }

        public int Years { get; set; }

        [StringLength(16)]
        public string IsGraduated { get; set; }

        public bool GED { get; set; }
        public bool IsEnrolled { get; set; }

        
    }
}