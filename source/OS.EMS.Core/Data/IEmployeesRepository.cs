using OS.EMS.Model;

namespace OS.EMS.Data
{
    public interface IEmployeeRepository : IRepository<Employee>
    {

    }
}