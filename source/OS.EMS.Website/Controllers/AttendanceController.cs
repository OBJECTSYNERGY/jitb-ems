﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LinqToExcel;
using LinqToExcel.Domain;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Services;
using OS.EMS.Model.Helper;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{
    public class AttendanceController : BaseController
    {

        [Inject]
        public AttendanceController(IAttendanceService storeService, IAttendanceDataService attendanceDataService, IEmployeeService employeeService, IGeographyService geographyService)
        {
            AttendanceService = storeService;
            AttendanceDataService = attendanceDataService;
            EmployeeService = employeeService;
            GeographyService = geographyService;

        }

        public ActionResult Create()
        {
            var viewModel = new AttendanceVM();
            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Create(AttendanceVM viewModel)
        {
            if (viewModel == null) return RedirectToAction("InvalidRequest", "Error");

            var store = viewModel.ToModel();
            if (AttendanceService.Create(store))
            {
                ShowMessage(Messages.CreateAttendanceSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult Update(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var store = AttendanceService.Get(id.Value);
            var viewModel = store.ToModelVM();
            return View("CreateOrUpdate", viewModel);
        }

        public ActionResult ListAttendanceData()
        {
            var attendanceData = AttendanceDataService.GetAll().OrderByDescending(a => a.Time).ThenBy(a=>a.Name);
            return View(attendanceData);
        }

        public ActionResult List()
        {
            var attendance = AttendanceService.GetAll();
            return View(attendance);
        }

        public ActionResult ImportExcel()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ImportExcel(FormCollection frmColl)
        {

            if (Request.Files["FileUpload1"].ContentLength > 0)
            {
                string extension = System.IO.Path.GetExtension(Request.Files["FileUpload1"].FileName);
                string fileName = System.IO.Path.GetFileName(Request.Files["FileUpload1"].FileName);

                string filePath = string.Format("{0}/{1}", Server.MapPath(AppConstants.AttendanceFilesPath), fileName);
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);

                Request.Files["FileUpload1"].SaveAs(filePath);

                var excel = new ExcelQueryFactory(filePath);
                //excel.DatabaseEngine = DatabaseEngine.Ace;
                //var columnNames = excel.GetColumnNames("Sheet1"); 
                excel.AddMapping("AcountNo", "AC-No#");

                string worksheetName = "Sheet1";
                int worksheetIndex = 0;

                var attendanceData = excel.Worksheet<AttendanceData>(worksheetIndex).OrderBy(a => a.AcountNo).ToList();
                
                var cnt = attendanceData.Count();

                bool statusCreate = false;
                foreach (var data in attendanceData)
                {
                    statusCreate = AttendanceDataService.Create(data);
                }

                
                if (statusCreate)
                {
                    ShowMessage(Messages.CreateAttendanceSuccess, MessageType.Success);
                    return RedirectToAction("ListAttendanceData");
                }
                
            }

            return RedirectToAction("InvalidRequest", "Error");
        }

        
    }
}
