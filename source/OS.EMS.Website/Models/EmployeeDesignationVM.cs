﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Web.Models
{
    public class EmployeeDesignationVM
    {
        public Guid? Id { get; set; }
        public bool IsActive { get; set; }

        [StringLength(64), MaxLength(64)]
        public string Designation { get; set; }
    }

    public static class EmployeeDesignationVMExtensions
    {

        public static EmployeeDesignationVM ToModelVM(this EmployeeDesignation model)
        {
            var viewModel = new EmployeeDesignationVM();
            Utils.Map(model, viewModel);
            return viewModel;

        }


        public static List<EmployeeDesignationVM> ToEmployeeDesignationVM(this IEnumerable<EmployeeDesignation> model)
        {
            var viewModel = new List<EmployeeDesignationVM>();
            Utils.Map(model, viewModel);
            return viewModel;
        }


        public static EmployeeDesignation ToModel(this EmployeeDesignationVM viewModel, EmployeeDesignation user = null)
        {
            if (user == null) user = new EmployeeDesignation();

            Utils.Map(viewModel, user);
            return user;
        }

    }

}