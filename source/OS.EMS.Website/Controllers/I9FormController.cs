﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Services;
using OS.EMS.Model.Helper;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{
    public class I9FormController : BaseController
    {

        [Inject]
        public I9FormController(II9FormService i9FormService, IEmployeeService employeeService, IApplicantService applicantService, IStoreService storeService, IAttendanceService attendanceService, IGeographyService geographyService, IAcceptableDocumentServices acceptableDocumentService)
        {
            ApplicantService = applicantService;
            EmployeeService = employeeService;
            AttendanceService = attendanceService;
            StoreService = storeService;
            GeographyService = geographyService;
            I9FormService = i9FormService;
            AcceptableDocumentService = acceptableDocumentService;
        }


        #region CRUD Operations
        public ActionResult Create(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var model = I9FormService.GetByEmployeeId(id.Value);
            var viewModel = model.ToViewModel();

            //Init Acceptable Documents
            InitAcceptableDoc(viewModel);

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Create(I9FormUSVM viewModel, FormCollection frmColl)
        {
            if (viewModel == null) return RedirectToAction("InvalidRequest", "Error");

            var I9FormUS = viewModel.ToModel();

            if (I9FormService.Create(I9FormUS))
            {

                //FOr Attach Documents
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;


                    string dirPath = Server.MapPath(AppConstants.EmpDocPath + I9FormUS.Id);

                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }

                    var newFileName = file + Path.GetExtension(hpf.FileName);
                    string savedFileName = Path.Combine(dirPath, newFileName);
                    hpf.SaveAs(savedFileName);
                }



                ShowMessage(Messages.CreateFormW7Success, MessageType.Success);
                //return SearchByExt("I9Form", new {id = form.EmployeeId});
                return RedirectToAction("Details", "Employee", new { id = I9FormUS.EmployeeId });
            }

            return RedirectToAction("InvalidRequest", "Error");
        }

        public ActionResult Update(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var model = I9FormService.Get(id.Value);
            var viewModel = model.ToViewModel();

            if (viewModel.DocumentsA != null)
            {
                foreach (var item in viewModel.DocumentsA)
                {
                    item.DocumentType = GetAcceptableDocumentSLI();
                }
            }

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Update(I9FormUSVM viewModel)
        {
            if (viewModel == null || viewModel.Id == null) return RedirectToAction("InvalidRequest", "Error");

            var model = viewModel.ToModel();
            if (I9FormService.Update(model))
            {
                //For Accep Doc update
                foreach (var acceptableDocument in viewModel.DocumentsA.ToModel())
                {
                    AcceptableDocumentService.Update(acceptableDocument);
                }

                //For Attach Documents
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;


                    string dirPath = Server.MapPath(AppConstants.EmpDocPath + model.Id);

                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }

                    var newFileName = file + Path.GetExtension(hpf.FileName);
                    string savedFileName = Path.Combine(dirPath, newFileName);

                    //For Existing File
                    DirectoryInfo di = new DirectoryInfo(dirPath);
                    if (di.Exists)
                    {
                        FileInfo[] files = di.GetFiles(file + "*");
                        if (files != null && files.Any())
                        {
                            foreach (var fileInfo in files)
                            {
                                if (fileInfo.Exists)
                                {
                                    fileInfo.Delete();
                                }
                            }
                        }
                    }

                    hpf.SaveAs(savedFileName);
                }

                ShowMessage(Messages.UpdateI9Success, MessageType.Success);
                return RedirectToAction("Details", "Employee", new { id = model.EmployeeId });
            }

            return RedirectToAction("InvalidRequest", "Error");
        }

        #endregion

        #region AppHelper

        private void InitAcceptableDoc(I9FormUSVM viewModel)
        {
            var itm = GetAcceptableDocumentSLI();

            for (int i = 0; i < 6; i++)
            {
                var acceptableDocument = new AcceptableDocumentVM { DocumentType = itm, DocumentCode = "DC" + Guid.NewGuid() };
                viewModel.DocumentsA.Add(acceptableDocument);
            }

        }

        public List<SelectListItem> GetAcceptableDocumentSLI()
        {
            var acceptableDocumentSLI = new List<SelectListItem>
            {
                new SelectListItem {Value = "List A", Text = "List A"},
                new SelectListItem {Value = "List B", Text = "List B"},
                new SelectListItem {Value = "List C", Text = "List C"}
            };

            return acceptableDocumentSLI;
        }

        #endregion

    }


}