﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OS.EMS.Model;

namespace OS.EMS.Web.Models
{
    public class AttendanceDataVM
    {
        [StringLength(32), MaxLength(32)]
        public string AcountNo { get; set; }

        [StringLength(32), MaxLength(32)]
        public string Name { get; set; }
        
        public DateTime Time { get; set; }

        [StringLength(32), MaxLength(32)]
        public string State { get; set; }

        public string Exception { get; set; }
        
    }



}