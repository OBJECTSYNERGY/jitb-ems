using System.Web.Mvc;
using OS.EMS.Model;
using OS.EMS.Security;
using OS.EMS.Services;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{
    public class BaseController : Controller
    {

        protected IGeographyService GeographyService { get; set; }

        protected ISecurityService SecurityService { get; set; }
        protected ILoginService LoginService { get; set; }

        protected IStoreService StoreService { get; set; }

        protected IApplicantService ApplicantService { get; set; }
        protected IAcceptableDocumentServices AcceptableDocumentService { get; set; }

        protected IEmployeeService EmployeeService { get; set; }
        protected II9FormService I9FormService { get; set; }
        protected IFormW4Service FormW4Service { get; set; }

        protected IAttendanceService AttendanceService { get; set; }
        protected IAttendanceDataService AttendanceDataService { get; set; }
        protected IEmployeeDesignationService EmployeeDesignationService { get; set; }


        

        public BaseController()
        {

            //IsSessionExists();
        }


        public void ShowMessage(string text, string messageType)
        {
            var message = new MessageVM() { Text = text, Type = messageType };
            TempData["Message"] = message;
        }

        public void IsSessionExists()
        {
            var user = Session["AppUser"] as SystemUser;
            if (user == null)
            {
                RedirectToAction("Index", "Home");
            }
        }
    }

    public class MessageType
    {
        //public static readonly string Success = "success";
        public static readonly string Success = "warning";
        public static readonly string Error = "danger";
        //public static readonly string Danger = "danger";
        public static readonly string Warning = "warning";
    }

    public class Messages
    {
        //Login
        public const string InvalidEmailPassword = "Invalid Email Address or Password";

        //System User
        public const string CreateUserSuccess = "User has been created sucessfully";
        public const string UpdateUserSuccess = "User has been updated sucessfully";
        public const string ActivateUserSuccess = "User has been activated sucessfully";
        public const string DeactivateUserSuccess = "User has been inactivated sucessfully";

        //Stores
        public const string CreateStoreSuccess = "Store has been created sucessfully";
        public const string UpdateStoreSuccess = "Store has been updated sucessfully";
        public const string ActivateStoreSuccess = "Store has been activated sucessfully";
        public const string DeactivateStoreSuccess = "Store has been inactivated sucessfully";


        //Applicant
        public const string CreateApplicantSuccess = "Applicant has been created sucessfully";
        public const string UpdateApplicantSuccess = "Applicant has been updated sucessfully";
        public const string ActivateApplicantSuccess = "Applicant has been activated sucessfully";
        public const string DeactivateApplicantSuccess = "Applicant has been inactivated sucessfully";

        //Applicant FormI9
        public const string CreateW7Success = "I9 Form has been created sucessfully";
        public const string UpdateI9Success = "I9 Form has been updated sucessfully";

        //Applicant FormW7
        public const string CreateFormW7Success = "Form W7 has been created sucessfully";
        public const string UpdateFormW7Success = "Form W7 has been updated sucessfully";

        //Employee
        public const string CreateEmployeeSuccess = "Employee has been created sucessfully";
        public const string UpdateEmployeeSuccess = "Employee has been updated sucessfully";
        public const string ActivateEmployeeSuccess = "Employee has been activated sucessfully";
        public const string DeactivateEmployeeSuccess = "Employee has been inactivated sucessfully";
        public const string CreateEmployeei9Success = "Employee i9 Form has been created sucessfully";

        //Store
        public const string AssignStoreSuccess = "Applicant promoted to employee & Store has been assigned sucessfully";
        public const string AssignSupervisorStoreSuccess = "Supervisor has been associated sucessfully";

        //Attendance
        public const string CreateAttendanceSuccess = "Attendance has been created sucessfully";
        public const string UpdateAttendanceSuccess = "Attendance has been updated sucessfully";

        //Search
        public const string SearchSearchResults = "No record found for the specified search!";



        //Designation
        public const string CreateDesignationSuccess = "Designation has been created sucessfully";
        public const string UpdateDesignationSuccess = "Designation has been updated sucessfully";
        public const string ActivateDesignationSuccess = "Designation has been activated sucessfully";
        public const string DeactivateDesignationSuccess = "Designation has been inactivated sucessfully";
    }
}

