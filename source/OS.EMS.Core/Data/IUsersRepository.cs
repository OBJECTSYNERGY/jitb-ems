using OS.EMS.Model;

namespace OS.EMS.Data
{
    public interface ISystemUsersRepository : IRepository<SystemUser>
    {

    }
}