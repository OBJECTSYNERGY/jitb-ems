﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OS.EMS.Model.Helper
{
    public static class AppConstants
    {
        //For Code
        public const string ApplicantPrefix = "JITB-APP";
        public const string EmployeePrefix = "JITB-EMP";
        public const string StorePrefix = "JITB-STR";
        public const string ApplicationSeed = "9000";

        //For Url
        public const string AttendanceFilesPath = "~/Assets/AttendanceFiles/";
        public const string EmpDocPath = "~/Assets/EmpDoc/";

    }
}