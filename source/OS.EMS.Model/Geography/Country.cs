using System.ComponentModel.DataAnnotations;

namespace OS.EMS.Model
{
    public class Country : Entity
    {
        [StringLength(32)]
        public string Name { get; set; }

        [StringLength(32)]
        public string Code { get; set; }
    }
}