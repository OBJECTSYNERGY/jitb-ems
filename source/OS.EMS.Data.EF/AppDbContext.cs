﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using OS.EMS.Data.Configurations;
using OS.EMS.Model;

namespace OS.EMS.Data
{
    public class AppDbContext : DbContext 
    {
        // ToDo: Move Initializer to Global.asax; don't want dependence on SampleData
        //static AppDbContext()
        //{
            
        //}

        public AppDbContext(): base(nameOrConnectionString: "AppDB")
        {
            Database.SetInitializer(new AppDatabaseInitializer());
        }


        protected override void OnModelCreating(DbModelBuilder builder)
        {
            // Use singular table names
            builder.Conventions.Remove<PluralizingTableNameConvention>();
            builder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(AppDbContext)));

            //builder.Configurations.Add(new RoleConfiguration());
            //builder.Configurations.Add(new UserConfiguration());
            //builder.Configurations.Add(new StoreConfiguration());
            //builder.Configurations.Add(new ApplicantConfiguration());

           // builder.Entity<Store>().Property(p => p.Name).HasMaxLength(30);

        }

        public DbSet<AppCode> AppCodes { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<City> Cities { get; set; }

        public DbSet<Role> Roles { get; set; }
        public DbSet<SystemUser> SystemUsers { get; set; }

        public DbSet<EmployeeDesignation> EmployeeDesignation { get; set; }
        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<EducationType> EducationTypes { get; set; }
        public DbSet<ApplicantEducation> ApplicantEducations { get; set; }
        public DbSet<ApplicantInterview> ApplicantInterviews { get; set; }
        public DbSet<ApplicantWorkHistory> ApplicantWorkHistories { get; set; }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<I9FormUS> I9FormsUS { get; set; }
        public DbSet<FormW4> FormW4 { get; set; }
        

        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<AttendanceData> AttendancesData { get; set; }

        public DbSet<Store> Stores { get; set; }        

        public DbSet<AcceptableDocument> I9FormDocuments { get; set; }

    }
}