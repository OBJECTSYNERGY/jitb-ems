﻿using System;
using System.Web;
using OS.EMS.Model;
using OS.EMS.Security;
using System.Web.Security;

namespace OS.EMS.Web.Security
{
    public class LoginService : ILoginService
    {
        public SystemUser User { 
            get 
            {
                var user = HttpContext.Current.Session["AppUser"];
                return user != null ? (SystemUser) user : null;
            } 
        }

        public void SignIn(SystemUser user)
        {
            HttpContext.Current.Session["AppUser"] = user;
            FormsAuthentication.SetAuthCookie(user.Id.ToString(), true);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }


    }
}