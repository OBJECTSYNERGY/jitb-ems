﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using OS.EMS.Model;
using OS.EMS.Model.Helper;


namespace OS.EMS.Web.Models
{
    public class AcceptableDocumentVM
    {

        
        public Guid? Id { get; set; }

        public string DocumentCode { get; set; }

        public Guid I9FormId { get; set; }
        public I9FormUS I9FormUS { get; set; }

        public string DocumentTypeId { get; set; }

        [Display(Name = "Document Type")]
        public IEnumerable<SelectListItem> DocumentType { get; set; }

        [Display(Name = "Document Title")]
        public string Title { get; set; }

        [Display(Name = "Issuing Authority")]
        public string Authority { get; set; }

        [Display(Name = "Document Number")]
        public string Number { get; set; }

        [Display(Name = "Expiry Date")]
        public DateTime? ExpiryDate { get; set; }

        private static void InitAcceptableDoc(I9FormUSVM viewModel)
        {
            var itm = new List<SelectListItem>
                {
                    new SelectListItem {Value = "List A", Text = "List A"},
                    new SelectListItem {Value = "List B", Text = "List B"},
                    new SelectListItem {Value = "List B", Text = "List C"}
                };


            var acceptableDocument = new AcceptableDocumentVM { DocumentType = itm };
            viewModel.DocumentsA.Add(acceptableDocument);
            viewModel.DocumentsA.Add(acceptableDocument);
            viewModel.DocumentsA.Add(acceptableDocument);
            viewModel.DocumentsA.Add(acceptableDocument);
            viewModel.DocumentsA.Add(acceptableDocument);
        }

    }


    public static class AcceptableDocumentExtensions
    {
        
        

        public static AcceptableDocumentVM ToViewModel(this AcceptableDocument source)
        {
            var destination = new AcceptableDocumentVM();
            Utils.Map(source, destination);            
            return destination;
        }

        public static List<AcceptableDocumentVM> ToViewModel(this List<AcceptableDocument> model)
        {
            var viewModel = new List<AcceptableDocumentVM>();
            Utils.Map(model, viewModel);
            return viewModel;
        }

        public static AcceptableDocument ToModel(this AcceptableDocumentVM source, AcceptableDocument destination = null)
        {
            if (destination == null) destination = new AcceptableDocument();
            Utils.Map(source, destination);
            return destination;
        }

        public static List<AcceptableDocument> ToModel(this List<AcceptableDocumentVM> viewModels)
        {
            var models = new List<AcceptableDocument>();
            Utils.Map(viewModels, models);
            return models;
        }


        //private static ICollection<AcceptableDocumentVM> MapDocToViewModel(ICollection<AcceptableDocument> models)
        //{
        //    ICollection<AcceptableDocumentVM> viewModels = new Collection<AcceptableDocumentVM>();
        //    Utils.Map(models, viewModels);
        //    return viewModels;
        //}

    }







}