﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using OS.EMS.Model;

namespace OS.EMS.Data
{
    public class AppDatabaseInitializer :
        CreateDatabaseIfNotExists<AppDbContext>      // when model is stable
        //DropCreateDatabaseIfModelChanges<AppDbContext> // when iterating
    {

        protected override void Seed(AppDbContext context)
        {
            // Seed code here

            var countries = AddCountries(context);
            var states = AddStates(context, countries);
            var cities = AddCities(context, states);
            
            var roles = AddRoles(context);
            var users = AddUsers(context, roles);

            var positionDesired = AddEmployeeDesignation(context);
            //var applicants = AddApplicant(context, cities);
            AddEducationType(context);

            //var employees = AddEmployees(context, applicants, cities);
            var stores = AddStores(context, cities);

            Save(context, users[0]);

            //foreach (var employeeToEdit in context.Employees)
            //{
            //    if (stores == null && stores.Count > 0)
            //    {
            //        employeeToEdit.StoreId = stores[0].Id;
            //    }

            //    context.Employees.Add(employeeToEdit);

            //}

            //Save(context, users[0]);

        }

        private IList<AppCode> AddAppCode(AppDbContext context)
        {
            var appcodes = new List<AppCode>()
            {
                    new AppCode() { Id = Guid.NewGuid(), }
            };

            foreach (var appCode in appcodes)
            {
                context.AppCodes.Add(appCode);
            }

            return appcodes;
        }

        private IList<Country> AddCountries(AppDbContext context)
        {
            var countries = new List<Country>()
            {
                    new Country() { Id = Guid.NewGuid(), Name="United States", Code="US" }
            };

            foreach (var country in countries)
            {
                context.Countries.Add(country);
            }

            return countries;
        }
        private IList<State> AddStates(AppDbContext context, IList<Country> countries)
        {
            var states = new List<State>(){
                new State() { Id=Guid.NewGuid(), Name="Texas", Code="TX", CountryId = countries[0].Id }
            };

            foreach (var state in states)
            {
                context.States.Add(state);
            }

            return states;

        }
        private IList<City> AddCities(AppDbContext context, IList<State> states)
        {
            var cities = new List<City>(){
                new City() { Id=Guid.NewGuid(), Name="Abilene", Code="Abilene", StateId = states[0].Id, CountryId = states[0].CountryId },
                new City() { Id=Guid.NewGuid(), Name="Addison", Code="Addison", StateId = states[0].Id, CountryId = states[0].CountryId },
                new City() { Id=Guid.NewGuid(), Name="Alamo", Code="Alamo", StateId = states[0].Id, CountryId = states[0].CountryId },
                new City() { Id=Guid.NewGuid(), Name="Alamo Height", Code="Alamo Height", StateId = states[0].Id, CountryId = states[0].CountryId },
                new City() { Id=Guid.NewGuid(), Name="Angelina County", Code="Angelina County", StateId = states[0].Id, CountryId = states[0].CountryId }
            };

            foreach (var city in cities)
            {
                context.Cities.Add(city);
            }

            return cities;
        }

        private IList<Role> AddRoles(AppDbContext context)
        {
            var roles = new List<Role>()
                {
                     new Role() { Id = Guid.NewGuid(), Name="Manager" },
                     new Role() { Id = Guid.NewGuid(), Name= "Admin" },
                };

            foreach (var role in roles)
            {
                context.Roles.Add(role);
            }

            return roles;
        }
        private IList<SystemUser> AddUsers(AppDbContext context, IList<Role> role)
        {
            var users = new List<SystemUser>()
            {
                new SystemUser() { Id = Guid.NewGuid(), FirstName="[System]", LastName="[System]",  Email = "admin", Password = "admin127", RoleId = role[1].Id, CreatedOn = DateTime.Now, ModifiedOn = null, DeletedOn = null, IsActive = true },
                new SystemUser() { Id = Guid.NewGuid(), FirstName="Derrick", LastName="Alexander",  Email = "sa", Password = "sa101", RoleId = role[1].Id, CreatedOn = DateTime.Now, ModifiedOn = null, DeletedOn = null, IsActive = true }
                //new SystemUser() { Id = Guid.NewGuid(), FirstName="S", LastName="SA",  Email = "sa", Password = "sa125", RoleId = role[1].Id, CreatedOn = DateTime.Now, ModifiedOn = null, DeletedOn = null, IsActive = true },
                //new SystemUser() { Id = Guid.NewGuid(), FirstName="Derrick", LastName="Alexander",  Email = "da", Password = "dm124", RoleId = role[0].Id, CreatedOn = DateTime.Now, ModifiedOn = null, DeletedOn = null, IsActive = true },
                //new SystemUser() { Id = Guid.NewGuid(), FirstName="Derrick", LastName="Alexander",  Email = "da", Password = "da124", RoleId = role[1].Id, CreatedOn = DateTime.Now, ModifiedOn = null, DeletedOn = null, IsActive = true }
            };

            foreach (var user in users)
            {
                context.SystemUsers.Add(user);
            }

            return users;
        }

        private IList<EducationType> AddEducationType(AppDbContext context)
        {
            var educationTypes = new List<EducationType>()
                {
                     new EducationType() { Id = Guid.NewGuid(), Code = "HighSchool", IsActive = true, Title = "High School Education"},
                     new EducationType() { Id = Guid.NewGuid(), Code = "CollegeSchool", IsActive = true, Title = "College Education"},
                     new EducationType() { Id = Guid.NewGuid(), Code = "Techical", IsActive = true, Title = "Techical Education"},
                };

            foreach (var type in educationTypes)
            {
                context.EducationTypes.Add(type);
            }

            return educationTypes;
        }
        private IList<EmployeeDesignation> AddEmployeeDesignation(AppDbContext context)
        {
            var positionDesired = new List<EmployeeDesignation>()
                {
                    new EmployeeDesignation() { Id = Guid.NewGuid(), Designation = "Staff", IsActive = true},
                    new EmployeeDesignation() { Id = Guid.NewGuid(), Designation= "Cashier", IsActive = true },
                    new EmployeeDesignation() { Id = Guid.NewGuid(), Designation= "Cook", IsActive = true },
                    new EmployeeDesignation() { Id = Guid.NewGuid(), Designation= "Shop Leader", IsActive = true },
                    new EmployeeDesignation() { Id = Guid.NewGuid(), Designation= "Manager", IsActive = true },
                    new EmployeeDesignation() { Id = Guid.NewGuid(), Designation= "Assistant Manager", IsActive = true },
                    new EmployeeDesignation() { Id = Guid.NewGuid(), Designation = "District Manager", IsActive = true }
                };

            foreach (var position in positionDesired)
            {
                context.EmployeeDesignation.Add(position);
            }

            return positionDesired;
        }
        private IList<Applicant> AddApplicant(AppDbContext context, IList<City> cities)
        {
            var applicants = new List<Applicant>()
                {
                     new Applicant() { Id = Guid.NewGuid(),  Code = "JITB-APP9001", LastName  = "Derrick1", FirstName = "Alexander", Address = "1862, Naper ville", StateId = cities[0].StateId, CityId = cities[0].Id, ZipCode  = "98005", Phone = "(999)999-9999" , Email="sa@sa.com", DateOfBirth = DateTime.UtcNow, DateOfAvailability = DateTime.UtcNow, PreviousWorkedFrom = DateTime.UtcNow, PreviousWorkedTo = DateTime.UtcNow, SesonalEmploymentFrom = DateTime.UtcNow, SesonalEmploymentTo = DateTime.UtcNow},
                     //new Applicant() { Id = Guid.NewGuid(), Code = "JITB-APP9002", LastName  = "Ana", FirstName = "Stern", Address = "1862, Naper ville", StateId = cities[0].StateId, CityId = cities[0].Id, ZipCode  = "98005", Phone = "999123456", DateOfBirth = DateTime.UtcNow, DateOfAvailability = DateTime.UtcNow, PreviousWorkedFrom = DateTime.UtcNow, PreviousWorkedTo = DateTime.UtcNow, SesonalEmploymentFrom = DateTime.UtcNow, SesonalEmploymentTo = DateTime.UtcNow},
                     //new Applicant() { Id = Guid.NewGuid(), Code = "JITB-APP9003", LastName  = "SA", FirstName = "Q", Address = "1862, Naper ville",StateId = cities[0].StateId, CityId = cities[0].Id, ZipCode  = "98005", Phone = "999123456", DateOfBirth = DateTime.UtcNow, DateOfAvailability = DateTime.UtcNow, PreviousWorkedFrom = DateTime.UtcNow, PreviousWorkedTo = DateTime.UtcNow, SesonalEmploymentFrom = DateTime.UtcNow, SesonalEmploymentTo = DateTime.UtcNow}
                };

            foreach (var applicant in applicants)
            {
                context.Applicants.Add(applicant);
            }

            return applicants;
        }

        private IList<Employee> AddEmployees(AppDbContext context, IList<Applicant> applicants, IList<City> cities)
        {
            var employees = new List<Employee>()
                {
                    new Employee() { Id = Guid.NewGuid(), Code = "Emp999", ApplicantId  = applicants[0].Id ,LastName  = "SA", FirstName = "Q", Address = "1862, Naper ville", StateId = cities[0].StateId, CityId = cities[0].Id, Email = "info@user.com", ZipCode  = "98005", Phone = "999123456",DateOfBirth = DateTime.UtcNow} ,
                    new Employee() { Id = Guid.NewGuid(), Code = "Emp888", ApplicantId  = applicants[1].Id, LastName  = "Ana", FirstName = "Stern", Address = "1862, Naper ville", StateId = cities[0].StateId, CityId = cities[0].Id, Email = "info@user.com", ZipCode  = "98005", Phone = "999123456", DateOfBirth = DateTime.UtcNow},
                    new Employee() { Id = Guid.NewGuid(), Code = "Emp777", ApplicantId  = applicants[2].Id, LastName  = "Derrick", FirstName = "Alexander", Address = "1862, Naper ville", StateId = cities[0].StateId, CityId = cities[0].Id, Email = "info@user.com", ZipCode  = "98005", Phone = "999123456", DateOfBirth = DateTime.UtcNow}
                     
                };

            foreach (var emp in employees)
            {
                context.Employees.Add(emp);
            }

            HttpContext.Current.Application.Add("DefaultEmp", employees[0]);

            return employees;
        }

        private IList<Store> AddStores(AppDbContext context, IList<City> cities)
        {
            var stores = new List<Store>()
                {
                    new Store() { Id = Guid.NewGuid(), Code = "JITB-STR9001", Name = "JITB Angelina County", Address = "1862, Naper ville", Email = "info@jitb.com", IsActive = true, Phone = "999999999", StateId = cities[0].StateId, CityId = cities[0].Id, ZipCode = "98006", CreatedOn = DateTime.Now.AddMinutes(-1)},
                    //new Store() { Id = Guid.NewGuid(), Code = "JITB-STR9002", Name = "JITB Alamo Height", Address = "1862, Naper ville", Email = "info@jitb.com", IsActive = true, Phone = "999999999", StateId = cities[1].StateId, CityId = cities[1].Id, ZipCode = "98006", CreatedOn = DateTime.Now},
                    //new Store() { Id = Guid.NewGuid(), Code = "JITB-STR9003", Name = "JITB Abilene", Address = "1862, Naper ville", Email = "info@jitb.com", IsActive = true, Phone = "999999999", StateId = cities[2].StateId, CityId = cities[2].Id, ZipCode = "98006", SupervisorId = null}
                };

            foreach (var model in stores)
            {
                context.Stores.Add(model);
            }

            return stores;
        }

        

    
        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        private void Save(AppDbContext context, SystemUser user)
        {
            // var auditUser = HttpContext.Current.User.Identity.Name;
            DateTime auditDate = DateTime.Now;

            foreach (DbEntityEntry<IAuditEntity> entry in context.ChangeTracker.Entries<IAuditEntity>())
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.CreatedOn = auditDate;
                    entry.Entity.CreatedById = user.Id;
                }
                else if (entry.State == EntityState.Modified)
                {
                    entry.Entity.ModifiedOn = auditDate;
                    entry.Entity.ModifiedById = user.Id;
                }
                else if (entry.State == EntityState.Deleted)
                {
                    entry.State = EntityState.Modified;
                    entry.Entity.DeletedOn = auditDate;
                    entry.Entity.DeletedById = user.Id;
                }
            }

            context.SaveChanges();
        }

    }
}