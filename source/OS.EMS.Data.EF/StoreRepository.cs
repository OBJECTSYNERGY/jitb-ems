﻿using System.Data.Entity;
using System.Linq;
using OS.EMS.Model;

namespace OS.EMS.Data
{
    public class StoreRepository : EFRepository<Store>, IStoresRepository
    {
        public StoreRepository(DbContext context) : base(context) { }

        //Commit because DbContext.Configuration.ProxyCreationEnabled = true; in AppNow - by SA
        //public override IQueryable<Store> GetAll()
        //{
        //    return DbSet.Include("City").Include("State").Include("Supervisor");
        //}

        public override Store GetById(System.Guid id)
        {
            return GetAll().SingleOrDefault(s => s.Id == id);
        }
    }
}
