﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OS.EMS.Model;

namespace OS.EMS.Data
{
    public class SystemUsersRepository : EFRepository<SystemUser>, ISystemUsersRepository
    {
        public SystemUsersRepository(DbContext context) : base(context) { }

        public override IQueryable<SystemUser> GetAll()
        {
            //return DbSet.Include("Role").Include("Employee");
            return DbSet.Include("Role");
            
        }
        
        public override SystemUser GetById(Guid id)
        {
            return GetAll().SingleOrDefault(s => s.Id == id);
        }
        
    }
}
