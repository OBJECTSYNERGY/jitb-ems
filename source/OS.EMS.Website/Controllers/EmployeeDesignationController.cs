﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Services;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{
    public class EmployeeDesignationController : BaseController
    {
        [Inject]
        public EmployeeDesignationController(IEmployeeDesignationService positionDesiredService)
        {
            EmployeeDesignationService = positionDesiredService;
        }

        #region CRUD Operations

        public ActionResult Create()
        {
            var viewModel = new EmployeeDesignationVM();
            return View("CreateOrUpdate", viewModel);

        }

        [HttpPost]
        public ActionResult Create(EmployeeDesignationVM viewModel)
        {
            if (viewModel == null) return RedirectToAction("InvalidRequest", "Error");

            var employeeDesignation = viewModel.ToModel();

            if (EmployeeDesignationService.Create(employeeDesignation))
            {
                ShowMessage(Messages.CreateDesignationSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult Update(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var employeeDesignation = EmployeeDesignationService.Get(id.Value);
            var viewModel = employeeDesignation.ToModelVM();
            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Update(EmployeeDesignationVM viewModel)
        {
            if (viewModel == null || viewModel.Id == null) return RedirectToAction("InvalidRequest", "Error");

            var store = viewModel.ToModel();

            if (EmployeeDesignationService.Update(store))
            {
                ShowMessage(Messages.UpdateDesignationSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult List()
        {
            var models = EmployeeDesignationService.GetAll();
            var viewModels = models.ToEmployeeDesignationVM();
            return View(viewModels);
        }

        public ActionResult Activate(Guid? id, bool active)
        {
            if (!id.HasValue) return RedirectToAction("InvalidRequest", "Error");

            if (EmployeeDesignationService.Activate(id.Value, active))
            {
                ShowMessage(active ? Messages.ActivateDesignationSuccess : Messages.DeactivateDesignationSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("List");
        }
        #endregion
    }
}
