﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OS.EMS.Model;

namespace OS.EMS.Web.Models
{
    public class AttendanceInfoVM
    {
        public AttendanceInfoVM()
        {
            FromDate = new DateTime().Date;
            ToDate = new DateTime().AddHours(24).Date;
        }

        public bool IsDefaultAttendace { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        
        public Guid? EmployeeId { get; set; }

        public double SaleryPayable { get; set; }

        public double SaleryRate { get; set; }

        public double WorkingHours { get; set; }

        //For Attendance
        public IEnumerable<AttendanceData> AttendancesToday{ get; set; }
        public IEnumerable<AttendanceData> AttendancesYesterday { get; set; }
        public IEnumerable<AttendanceData> AttendancesSearched { get; set; }
        
    }
  
}