﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OS.EMS.Model.Helper
{
    public static class Utils
    {
        public static T2 Map<T1, T2>(T1 source, T2 destination)
        {
            return (T2)AutoMapper.Mapper.Map(source, destination, typeof(T1), typeof(T2), o => o.CreateMissingTypeMaps = true);
        }
    }
}