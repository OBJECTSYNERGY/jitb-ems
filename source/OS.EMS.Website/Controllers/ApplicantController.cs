﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Services;
using OS.EMS.Model.Helper;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{
    public class ApplicantController : BaseController
    {

        [Inject]
        public ApplicantController(IApplicantService applicantService, IEmployeeService employeeServices, IAttendanceService attendanceServices, IStoreService storeService, IGeographyService geographyService, IEmployeeDesignationService positionDesiredService)
        {
            ApplicantService = applicantService;
            EmployeeDesignationService = positionDesiredService;
            EmployeeService = employeeServices;
            AttendanceService = attendanceServices;
            StoreService = storeService;
            GeographyService = geographyService;

        }

        #region CRUD Operations

        public ActionResult Create()
        {
            var viewModel = new ApplicantVM();
            PopulateGeographySLI(viewModel);

            //For EmployeeDesignation
            ForPositionDesireSLI(viewModel);

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Create(ApplicantVM viewModel)
        {
            if (viewModel == null) return RedirectToAction("InvalidRequest", "Error");

            var applicant = viewModel.ToModel();

            if (ApplicantService.Create(applicant))
            {
                ShowMessage(Messages.CreateApplicantSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult Update(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var model = ApplicantService.Get(id.Value);
            var viewModel = model.ToApplicantVM();

            //For EmploymentType SLI at Education Listing
            if (viewModel.Educations != null)
            {
                SetEducationTypeSLI(viewModel);
            }

            //For City & State SLI at WorkHistory Listing
            if (viewModel.ApplicantWorkHistories != null)
            {
                SetCityAndStateSLI(viewModel);
            }


            PopulateGeographySLI(viewModel);

            //For EmployeeDesignation
            ForPositionDesireSLI(viewModel);

            //For EmployeeID in view
            var employee = EmployeeService.GetByApplicantId(viewModel.Id.Value);
            if (employee != null && employee.Any())
            {
                viewModel.EmployeeId = employee.FirstOrDefault().Id;
            }

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Update(ApplicantVM viewModel)
        {
            if (viewModel == null || viewModel.Id == null) return RedirectToAction("InvalidRequest", "Error");

            //var applicant = ApplicantService.Get(viewModel.Id.Value);
            var model = viewModel.ToModel();

            //For Education
            //if (model.Educations != null)
            //{
            //    //List<ApplicantEducation> newEducation = new List<ApplicantEducation>();
            //    foreach (var item in model.Educations)
            //    {
            //        if (item.Id == Guid.Empty)
            //        {
            //            item.ApplicantId = model.Id;
            //            //item.Id = Guid.NewGuid();

            //        }
            //    }
            //}



            if (ApplicantService.Update(model))
            {
                ShowMessage(Messages.UpdateApplicantSuccess, MessageType.Success);

                var employee = EmployeeService.GetByApplicantId(model.Id);
                if (employee != null && employee.Any())
                {
                    return RedirectToAction("Details", "Employee", new { id = employee.FirstOrDefault().Id });    
                }

                return RedirectToAction("List");

            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult List(bool? showActive)
        {
            //var models = showActive.HasValue ? ApplicantService.GetByActivatedType(showActive.Value) : ApplicantService.GetAll();
            var models = showActive.HasValue ? ApplicantService.GetByActivatedType(showActive.Value) : ApplicantService.GetByActivatedType(false);
            var viewModels = models.ToApplicantVM();

            //For Store Listing
            PopulateStoreSLI();

            return View(viewModels);
        }

        public ActionResult Activate(Guid? id, bool active)
        {
            if (!id.HasValue) return RedirectToAction("InvalidRequest", "Error");

            if (ApplicantService.Activate(id.Value, active))
            {
                ShowMessage(active ? Messages.ActivateApplicantSuccess : Messages.DeactivateApplicantSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("List");
        }

        public ActionResult GetEducationView(Guid? applicantId, int educationCounter)
        {
            var viewModel = new ApplicantEducationVM();

            var models = ApplicantService.GetEducationType();
            viewModel.EducationTypes = models.Select(e => new SelectListItem { Value = e.Id.ToString(), Text = e.Title });
            if (applicantId.HasValue)
            {
                viewModel.ApplicantId = applicantId.Value;
            }

            viewModel.IntCounter = educationCounter;
            return View("_Education", viewModel);
        }

        public ActionResult GetWorkHistoryView(Guid? applicantId, int workHistoryCounter)
        {
            var viewModel = new ApplicantWorkHistoryVM();
            viewModel.IntCounter = workHistoryCounter;
            if (applicantId.HasValue)
            {
                viewModel.ApplicantId = applicantId.Value;
            }
            PopulateGeographySLI(viewModel);

            return View("_WorkHistory", viewModel);
        }

        public ActionResult GetInterviewView(Guid? applicantId, int interviewCounter)
        {
            var viewModel = new ApplicantInterviewVM();
            viewModel.IntCounter = interviewCounter;
            if (applicantId.HasValue)
            {
                viewModel.ApplicantId = applicantId.Value;
            }
            return View("_Interview", viewModel);
        }

        [HttpPost]
        public ActionResult SearchByCode(string applicantCode)
        {
            if (string.IsNullOrEmpty(applicantCode))
            {
                return RedirectToAction("Dashboard", "Home");
            }

            var applicants = ApplicantService.SearchByCode(applicantCode);
            

            if (applicants.Count() == 1)
            {
                return RedirectToAction("Update", new {id = applicants.FirstOrDefault().Id});
                //var viewModel = aplicantsVM.FirstOrDefault();

                ////For EmploymentType SLI at Education Listing
                //if (viewModel.Educations != null)
                //{
                //    SetEducationTypeSLI(viewModel);
                //}

                ////For City & State SLI at WorkHistory Listing
                //if (viewModel.ApplicantWorkHistories != null)
                //{
                //    SetCityAndStateSLI(viewModel);
                //}


                //PopulateGeographySLI(viewModel);

                ////For EmployeeDesignation
                //ForPositionDesireSLI(viewModel);

                ////For EmployeeID in view
                //var employee = EmployeeService.GetByApplicantId(viewModel.Id.Value);
                //if (employee != null && employee.Any())
                //{
                //    viewModel.EmployeeId = employee.FirstOrDefault().Id;
                //}

                //return View("CreateOrUpdate", viewModel);


            }
            else if (applicants.Count() > 1)
            {
                var aplicantsVM = new List<ApplicantVM>();
                Utils.Map(applicants, aplicantsVM);

                ////For Store Listing
                PopulateStoreSLI();

                return View("List", aplicantsVM);
            }

            return RedirectToAction("Dashboard", "Home");
        }

        #endregion


        #region AppHelper

        private void PopulateGeographySLI(ApplicantVM viewModel)
        {
            var states = GeographyService.GetStates();
            viewModel.States = states.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = s.Name });

            var cities = GeographyService.GetCities();
            viewModel.Cities = cities.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
        }

        private void PopulateGeographySLI(ApplicantWorkHistoryVM viewModel)
        {
            var states = GeographyService.GetStates();
            viewModel.States = states.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = s.Name });

            var cities = GeographyService.GetCities();
            viewModel.Cities = cities.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
        }

        //private void ForApplicantCode(ApplicantVM viewModel)
        //{
        //    var applicant = ApplicantService.GetLast();
        //    if (applicant != null)
        //    {
        //        int applicantSufix = int.Parse(applicant.Code.Substring(AppConstants.ApplicantPrefix.Length));
        //        viewModel.Code = string.Format("{0}{1}", AppConstants.ApplicantPrefix, ++applicantSufix);
        //    }
        //    else
        //    {
        //        int applicationSeed = int.Parse(AppConstants.ApplicationSeed);
        //        viewModel.Code = string.Format("{0}{1}", AppConstants.ApplicantPrefix, ++applicationSeed);
        //    }
        //}

        private void PopulateStoreSLI()
        {
            //For Store Listing
            var stores = StoreService.GetAll();
            ViewData["StoresSLI"] = stores.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
        }

        private void ForPositionDesireSLI(ApplicantVM viewModel)
        {
            //For EmployeeDesignation Listing
            viewModel.PositionsDesiredSLI = EmployeeDesignationService.GetAll().Where(x => x.IsActive == true).Select(c => new SelectListItem { Value = c.Designation, Text = c.Designation });
        }

        //private void ForAttendanceTestRecords(Employee employee)
        //{
        //    //FOR ATTENDANCE JUST FOR TEST
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddDays(-7),
        //        SignOut = DateTime.Now.AddDays(-7).AddHours(6)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddDays(-6),
        //        SignOut = DateTime.Now.AddDays(-6).AddHours(6)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddDays(-5),
        //        SignOut = DateTime.Now.AddDays(-5).AddHours(6)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddDays(-4),
        //        SignOut = DateTime.Now.AddDays(-4).AddHours(6)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddDays(-3),
        //        SignOut = DateTime.Now.AddDays(-3).AddHours(6)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddDays(-2),
        //        SignOut = DateTime.Now.AddDays(-2).AddHours(6)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddDays(-1),
        //        SignOut = DateTime.Now.AddDays(-1).AddHours(6)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now,
        //        SignOut = DateTime.Now.AddHours(2)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddHours(3),
        //        SignOut = DateTime.Now.AddHours(5)
        //    });
        //    AttendanceService.Create(new Attendance()
        //    {
        //        Id = Guid.NewGuid(),
        //        EmployeeId = employee.Id,
        //        SignIn = DateTime.Now.AddHours(6),
        //        SignOut = DateTime.Now.AddHours(9)
        //    });
        //}

        private void SetCityAndStateSLI(ApplicantVM viewModel)
        {
            var cities = GeographyService.GetCities();
            var citiesSLI = cities.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });

            var states = GeographyService.GetStates();
            var statesSLI = states.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = s.Name });

            foreach (var item in viewModel.ApplicantWorkHistories)
            {
                item.Cities = citiesSLI;
                item.States = statesSLI;
            }
        }

        private void SetEducationTypeSLI(ApplicantVM viewModel)
        {
            var models = ApplicantService.GetEducationType();
            var eduTypeSSL = models.Select(e => new SelectListItem { Value = e.Id.ToString(), Text = e.Title });
            foreach (var item in viewModel.Educations)
            {
                item.EducationTypes = eduTypeSSL;
            }
        }


        [HttpGet]
        public virtual JsonResult IsEmailExists(string Email, string InitialEmail)
        {
            //Thread.Sleep(2000);//fake delay
            if (Email == InitialEmail)
            {
                return this.Json(true, JsonRequestBehavior.AllowGet);
            }
            return this.Json(!ApplicantService.IsEmailExists(Email), JsonRequestBehavior.AllowGet);
        }

        #endregion



    }
}


