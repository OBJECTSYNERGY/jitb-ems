﻿using System.Data.Entity;
using System.Linq;
using OS.EMS.Model;

namespace OS.EMS.Data
{
    public class EmployeeRepository : EFRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext context) : base(context) { }

        //public override IQueryable<Employee> GetAll()
        //{
        //    return DbSet.Include("City").Include("State");
        //   // return DbSet.Include("City").Include("State").Include("Applicant").Include("Store").Include("SystemUser");
        //}

        public override Employee GetById(System.Guid id)
        {
            return GetAll().SingleOrDefault(s => s.Id == id);
        }
    }
}
