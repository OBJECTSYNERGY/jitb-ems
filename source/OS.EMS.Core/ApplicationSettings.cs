﻿using System.Xml.Linq;

namespace OS.EMS
{
    public class ApplicationSettings : Settings, IApplicationSettings
    {

        public string CustomersFolder { get; set; }
  
        private const string _name = "app-settings";
        public override string Name
        {
            get { return _name; }
        }

        public override void Load(XElement settings)
        {
            CustomersFolder = settings.Element("customersfolder").Value;    
        }

        public override XElement ToXElement()
        {
            var settings = new XElement(_name);
            settings.Add(new XElement("customersfolder", CustomersFolder));

            return settings;
        }

        public override void SetDefaults()
        {
            CustomersFolder = string.Empty;
        }
    }

    public interface IApplicationSettings : ISettings
    {
        //string CustomersFolder { get; }
    }
}
