﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Web.Models
{
    
    public enum RoleType
    {
        Admin = 0,
        Manager
    }

    public class SystemUserVM
    {
        public Guid? Id { get; set; }

        public bool IsActive { get; set; }

        [Required()]
        public Guid? RoleId { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }

        [Required()]
        public Guid? EmployeeId { get; set; }
        public Employee Employee { get; set; }
        public IEnumerable<SelectListItem> Managers { get; set; }

        [Display(Name = "First Name"), Required(), StringLength(32), MaxLength(32)]
        public string FirstName { get; set; }

        [Display(Name = "Last Name"), Required(), StringLength(32), MaxLength(32)]
        public string LastName { get; set; }

        [Display(Name = "Email Address"), Required(), RegularExpression(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$", ErrorMessage = "Not a valid email address!"), StringLength(64), MaxLength(64)]
        [Remote("IsEmailExists", "User", ErrorMessage = "Email Address is not available", AdditionalFields = "InitialEmail")]
        public string Email { get; set; }

        [DataType(DataType.Password), Required(), StringLength(32), MaxLength(32)]
        public string Password { get; set; }

        [Display(Name = "Confirm Password"), DataType(DataType.Password), Required(), StringLength(32), MaxLength(32)]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessage = "The New Password and Confirm New Password fields did not match!")]
        public string PasswordConfirm { get; set; }
        
  
    }

 


    public static class SystemUserVMExtensions
    {

        public static SystemUserVM ToSystemUserVM(this SystemUser model)
        {

            var viewModel = new SystemUserVM();
            Utils.Map(model, viewModel);
            return viewModel;

        }

        public static SystemUser ToModel(this SystemUserVM viewModel, SystemUser user = null)
        {
            if(user == null) user = new SystemUser();

            Utils.Map(viewModel, user);
            return user;
        }

   }

}