using OS.EMS.Model;

namespace OS.EMS.Data
{
    public interface IStoresRepository : IRepository<Store>
    {

    }
}