﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OS.EMS.Web.Models
{
    public class DashboardVM
    {
        //For Pending Applicants
        public List<ApplicantVM> PendingApplicants { get; set; }
        public IEnumerable<SelectListItem> Stores { get; set; }
        
        //For Quick Stats
        public int PendingApplicantsCount { get; set; }
        public int ActiveEmployeeCount { get; set; }
        public int ActiveStoresCount { get; set; }
        public int SystemUsersCount { get; set; }


    }
}