﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Web.Models
{
    public class ApplicantVM
    {
        public ApplicantVM()
        {
            //IsOverAge = true;

            // ApplicantWorkHistory = new List<ApplicantWorkHistoryVM>();
            //Educations = new List<ApplicantEducationVM>();
            Interviews = new List<ApplicantInterviewVM>();
            ApplicantWorkHistories = new List<ApplicantWorkHistoryVM>();
        }

        public Guid? Id { get; set; }
        
        //For View Only nned to assign Implecitely
        public Guid? EmployeeId { get; set; }

        [Display(Name = "Store Code")]
        [Required(ErrorMessage = "Please specify Store Code!") , StringLength(32), MaxLength(32)]
        public string Code { get; set; }

        public bool IsActive { get; set; }

        //FOR PERSONAL 
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please specify Last Name!"), RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Only alphabets are allowed!"), StringLength(32), MaxLength(32)]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please specify First Name!"), RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Only alphabets are allowed!"), StringLength(32), MaxLength(32)]
        public string FirstName { get; set; }

        [Display(Name = "Middle Ini")]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Only alphabets are allowed!"), StringLength(16), MaxLength(16)]
        public string MiddleInitial { get; set; }

        [Display(Name = "Street Address")]
        [Required(ErrorMessage = "Please specify Street Address!"), StringLength(128), MaxLength(128)]
        public string Address { get; set; }

        public Guid? CityId { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }

        public Guid? StateId { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }

        [Required(ErrorMessage = "Please specify Zip Code!"), RegularExpression("^[0-9a-zA-Z-]+$", ErrorMessage = "Invalid Data!"), StringLength(16), MaxLength(16)]
        public string ZipCode { get; set; }

        [Display(Name = "Home Phone")]
        [Required(ErrorMessage = "Please specify Phone Number!"), RegularExpression("^[0-9()-]+$", ErrorMessage = "Phone Number should be numberic!"), StringLength(16), MaxLength(16)]
        public string Phone { get; set; }

        [Display(Name = "Email Address"), Required(), StringLength(64), MaxLength(64)]
        [Remote("IsEmailExists", "Applicant", ErrorMessage = "Email Address is not available", AdditionalFields = "InitialEmail")]
        public string Email { get; set; }
        
        [Display(Name = "Date of Birth"), Required(ErrorMessage = "Please specify Date of Birth!")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Please specify Overage!")]
        public bool? IsOverAge { get; set; }

        //Eligibility to work
        [Display(Name = "Is Elegible to Work"), Required(ErrorMessage = "Please specify elegible to work!")]
        public bool? CanProofElegiblity { get; set; }

        //Emergency Contact Person
        [Display(Name = "Contact Name"), StringLength(16), MaxLength(16)]
        public string EmergencyContactName { get; set; }

        [Display(Name = "Phone Number"), StringLength(16), MaxLength(16)]
        public string EmergencyPhone { get; set; }

        [Display(Name = "Address"), StringLength(128), MaxLength(128)]
        public string EmergencyAddress { get; set; }

        //FOR AVABILITY
        [StringLength(64), MaxLength(64)]
        public string ContactUsSource { get; set; }

        [Display(Name = "Position Desired")]
        [Required(ErrorMessage = "Please specify Position Desired!"), StringLength(64), MaxLength(64)]
        public string PositionsDesired { get; set; }
        public IEnumerable<SelectListItem> PositionsDesiredSLI { get; set; }


        [Display(Name = "Date of Avability")]
        public DateTime? DateOfAvailability { get; set; }

        [Display(Name = "Total Week Hours")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Only numbers are allowed!")]
        public int TotalWeekHours { get; set; }


        [Required(ErrorMessage = "Please specify Employment Type!")]
        public bool? IsSesonalEmployment { get; set; }

        //[Required(ErrorMessage = "Please specify seasonal employment From!")]
        public DateTime? SesonalEmploymentFrom { get; set; }

        //[Required(ErrorMessage = "Please specify seasonal Employment To!")]
        public DateTime? SesonalEmploymentTo { get; set; }

        //Does any thing prevent to report at work
        public bool? IsPreventToWorkDaily { get; set; }

        [StringLength(256), MaxLength(256)]
        public string PreventToWorkReason { get; set; }

        [StringLength(2), MaxLength(2)]
        public string MondayFrom { get; set; }

        [StringLength(2), MaxLength(2)]
        public string MondayTo { get; set; }

        [StringLength(2), MaxLength(2)]
        public string TuesdayFrom { get; set; }

        [StringLength(2), MaxLength(2)]
        public string TuesdayTo { get; set; }

        [StringLength(2), MaxLength(2)]
        public string WednesdayFrom { get; set; }

        [StringLength(2), MaxLength(2)]
        public string WednesdayTo { get; set; }

        [StringLength(2), MaxLength(2)]
        public string ThursdayFrom { get; set; }

        [StringLength(2), MaxLength(2)]
        public string ThursdayTo { get; set; }

        [StringLength(2), MaxLength(2)]
        public string FridayFrom { get; set; }

        [StringLength(2), MaxLength(2)]
        public string FridayTo { get; set; }

        [StringLength(2), MaxLength(2)]
        public string SaturdayFrom { get; set; }

        [StringLength(2), MaxLength(2)]
        public string SaturdayTo { get; set; }

        [StringLength(2), MaxLength(2)]
        public string SundayFrom { get; set; }

        [StringLength(2), MaxLength(2)]
        public string SundayTo { get; set; }
        
        ////FOR MISCELLANEOUS (Previous working detail, Relative WithUs, Crime)
        [Required(ErrorMessage = "Please specify Previously worked status!")]
        public bool? IsPreviouslyWorkedWithUs { get; set; }

        [Display(Name = "Previous Worked Store Code"), Required(),StringLength(16), MaxLength(16)]
        public string PreviousWorkedStoreCode { get; set; }

        [Display(Name = "Previous Worked Store Contact Number"), Required(), RegularExpression("^[0-9()-]+$", ErrorMessage = "Previous worked contact number should be numberic!"), StringLength(16), MaxLength(16)]
        public string PreviousWorkedContact { get; set; }

        public bool? IsCompanyRestaurant { get; set; }
        
        //[Required()]
        public DateTime? PreviousWorkedFrom { get; set; }

        //[Required()]
        public DateTime? PreviousWorkedTo { get; set; }

        [StringLength(256), MaxLength(256)]
        public string PreviousLeavingReason { get; set; }

        [StringLength(128), MaxLength(128)]
        public string PreviousLocation { get; set; }

        [StringLength(32), MaxLength(32)]
        public string PreviousSupervisor { get; set; }

        public bool? IsRelativeWorkedWithUs { get; set; }

        [StringLength(32), MaxLength(32)]
        public string RelativeName { get; set; }

        [StringLength(32), MaxLength(32)]
        public string RelativeRelation { get; set; }

        [StringLength(128), MaxLength(128)]
        public string RelativeLocation { get; set; }

        public bool? CanWorkWithoutAccomodation { get; set; }
        public bool? DoesAgreeSafetyRules { get; set; }

        public bool? HasDeniedDrivingLicense { get; set; }

        [StringLength(256), MaxLength(256)]
        public string DeniedDrivingLicenseReason { get; set; }

        public bool? HasConvictedCrimed { get; set; }
        public bool? IsConvictedPrisoned { get; set; }
        public bool? IsConvictedPrisonedReleased { get; set; }

        [StringLength(256), MaxLength(256)]
        public string ConvictedPrisonedReason { get; set; }

        [StringLength(64), MaxLength(64)]
        public string ConvictedPrisonedCharge { get; set; }

        [StringLength(32), MaxLength(32)]
        public string ConvictedPrisonedPlace { get; set; }

        [StringLength(64), MaxLength(64)]
        public string ConvictedPrisonedAction { get; set; }

        //FOR EDUCATION
        public ICollection<ApplicantEducationVM> Educations { get; set; }

        //FOR INTERVIEWS
        public ICollection<ApplicantInterviewVM> Interviews { get; set; }
        ////public virtual ICollection<EmployeeInterview> EmployeeInterview { get; set; }
        /// 
        /// ////FOR WORK HISTORY
        public bool IsEverWorkedBefore { get; set; }
        public ICollection<ApplicantWorkHistoryVM> ApplicantWorkHistories { get; set; }
       
    }
    
    public static class ApplicantVMExtensions
    {
        public static ApplicantVM ToApplicantVM(this Applicant source)
        {
            var destination = new ApplicantVM();
            Utils.Map(source, destination);
            
            ////FOR EDUCATION
            //foreach (var item in source.Educations)
            //{
            //    var education = new ApplicantEducationVM();
            //    Utils.Map(item, education);
            //    destination.Educations.Add(education);
            //}
            
            //For Interviews
            //foreach (var item in source.Interviews)
            //{
            //    var interview = new ApplicantInterviewVM();
            //    Utils.Map(item, interview);
            //    destination.Interviews.Add(interview);
            //}

            //FOR WORK HISTORY
            //foreach (var item in source.ApplicantWorkHistories)
            //{
            //    var history = new ApplicantWorkHistoryVM();
            //    Utils.Map(item, history);
            //    destination.ApplicantWorkHistories.Add(history);
            //}

            return destination;
        }

        public static List<ApplicantVM> ToApplicantVM(this IEnumerable<Applicant> model)
        {
            var viewModel = new List<ApplicantVM>();
            Utils.Map(model, viewModel);
            return viewModel;
        }

        public static Applicant ToModel(this ApplicantVM source, Applicant destination = null)
        {
            if (destination == null) destination = new Applicant();

            Utils.Map(source, destination);

            ////For Educations
            //if (source.Educations != null)
            //{
            //    foreach (var item in source.Educations)
            //    {
            //        var education = new ApplicantEducation();
            //        Utils.Map(item, education);
            //        destination.Educations.Add(education);
            //    }
            //}

            //For Interviews
            //if (source.Interviews != null)
            //{
            //    foreach (var item in source.Interviews)
            //    {
            //        var interview = new ApplicantInterview();
            //        Utils.Map(item, interview);
            //        destination.Interviews.Add(interview);
            //    }
            //}
            
            //For Work History
            //if (source.ApplicantWorkHistories != null)
            //{
            //    foreach (var item in source.ApplicantWorkHistories)
            //    {
            //        var history = new ApplicantWorkHistory();
            //        Utils.Map(item, history);
            //        destination.ApplicantWorkHistories.Add(history);
            //    }
            //}
            
            //For Return 
            return destination;
        }

    }

    

}