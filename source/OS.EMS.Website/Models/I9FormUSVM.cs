﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OS.EMS.Model;
using AutoMapper;
using System.Web.Mvc;
using OS.EMS.Model.Helper;

namespace OS.EMS.Web.Models
{
    public class I9FormUSVM
    {
        public I9FormUSVM()
        {
            DocumentsA = new List<AcceptableDocumentVM>();
            //DocumentsB = new Collection<AcceptableDocumentVM>();
            //DocumentsC = new Collection<AcceptableDocumentVM>();
        }

        public Guid? Id { get; set; }
        public Guid EmployeeId { get; set; }
        public bool IsActive { get; set; }

        //GENERAL INFO
        [Display(Name = "Last Name"), Required(), StringLength(32), MaxLength(32)]
        public string LastName { get; set; }

        [Display(Name = "First Name"), Required(), StringLength(32), MaxLength(32)]
        public string FirstName { get; set; }

        [Display(Name = "Middle Initial"), StringLength(16), MaxLength(16)]
        public string MiddleInitial { get; set; }

        [Display(Name = "Other Name"), StringLength(32), MaxLength(32)]
        public string OtherNames { get; set; }

        [Display(Name = "Address"), Required(), StringLength(128), MaxLength(128)]
        public string StreetAddress { get; set; }

        [Display(Name = "Apt. Number"), StringLength(8), MaxLength(8)]
        public string ApartmentNumber { get; set; }

        [Display(Name = "City"), Required(), StringLength(32), MaxLength(32)]
        public string City { get; set; }

        [Display(Name = "State"), Required(), StringLength(32), MaxLength(32)]
        public string State { get; set; }

        [Display(Name = "Zip Code"), Required(), StringLength(16), MaxLength(16)]
        public string ZipCode { get; set; }

        [Display(Name = "Date of Birth"), Required()]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "U.S Social Security Number"), Required(), StringLength(32), MaxLength(32)]
        public string SocialSecurityNumber { get; set; }

        [Display(Name = "Email Address"), Required(), RegularExpression(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$", ErrorMessage = "Not a valid email address!"), StringLength(64), MaxLength(64)]
        public string Email { get; set; }

        [Display(Name = "Phone Number"), Required(), RegularExpression("^[0-9()-]+$", ErrorMessage = "Phone Number should be numberic!"), StringLength(16), MaxLength(16)]
        public string Phone { get; set; }

        //FOR CITIZENSHIP
        [Display(Name = "Is citizen of the United States")]
        public bool IsCitizen { get; set; }

        [Display(Name = "Is non-citizen national of the United States")]
        public bool IsNonCitizenNational { get; set; }

        [Display(Name = "Is lawful permanent resident")]
        public bool IsLawfulResident { get; set; }

        [Display(Name = "An alien authorized to work until")]
        public bool IsAlienAuthorized { get; set; }

        [Display(Name = "Alien. Expiration date, if applicable, mm/dd/yyyy")]
        public DateTime?AlienAuthorizedExpiryDate { get; set; }

        [Display(Name = "Alien Registration Number/USCIS Number"), StringLength(32), MaxLength(32)]
        public string AlienRegistrationNumber { get; set; }

        [Display(Name = "Form I-94 Admission Number"), StringLength(32), MaxLength(32)]
        public string I9FormAdmissionNumber { get; set; }

        [Display(Name = "Foreign Passport Number"), StringLength(32), MaxLength(32)]
        public string ForeignPassportNumber { get; set; }

        [Display(Name = "Country of Issuance"),StringLength(32), MaxLength(32)]
        public string CountryOfIssuance { get; set; }

        [Display(Name = "Employee Signature Date")]
        public DateTime? SignatureDate { get; set; }

        //FOR PREPARER 
        [Display(Name = "Signature Date")]
        public DateTime? PreparerSignatureDate { get; set; }

        [Display(Name = "Last Name (Family Name)"), StringLength(32), MaxLength(32)]
        public string PreparerLastName { get; set; }

        [Display(Name = "First Name (Given Name)"), StringLength(32), MaxLength(32)]
        public string PreparerFirstName { get; set; }

        [Display(Name = "Address (Street Number and Name)"), StringLength(128), MaxLength(128)]
        public string PreparerStreetAddress { get; set; }

        [Display(Name = "City or Town"), StringLength(32), MaxLength(32)]
        public string PreparerCity { get; set; }

        [Display(Name = "State"), StringLength(32), MaxLength(32)]
        public string PreparerState { get; set; }

        [Display(Name = "Zip Code"), StringLength(32), MaxLength(32)]
        public string PreparerZipCode { get; set; }

        //[Display(Name = "List A")]
        public List<AcceptableDocumentVM> DocumentsA { get; set; }

        //[Display(Name = "List B")]
        //public ICollection<AcceptableDocumentVM> DocumentsB { get; set; }

        //[Display(Name = "List C")]
        //public ICollection<AcceptableDocumentVM> DocumentsC { get; set; }


        [Display(Name = "First Day Date")]
        public DateTime? FirstDayDate { get; set; }

        //FOR EMPLOYER AUTHORIZATION/CERTIFICATION
        [Display(Name = "Signature Date")]
        public DateTime? RepresentativeSignatureDate { get; set; }

        [Display(Name = "Employer or Authorize Representative Title"), StringLength(32), MaxLength(32)]
        public string RepresentativeTitle { get; set; }

        [Display(Name = "Last Name (Family Name)"), StringLength(32), MaxLength(32)]
        public string RepresentativeLastName { get; set; }

        [Display(Name = "First Name (Given Name)"), StringLength(32), MaxLength(32)]
        public string RepresentativeFirstName { get; set; }

        [Display(Name = "Business or Organization Name"), StringLength(32), MaxLength(32)]
        public string RepresentativeOrganisation { get; set; }

        [Display(Name = "Street Address"), StringLength(128), MaxLength(128)]
        public string RepresentativeStreetAddress { get; set; }

        [Display(Name = "City or Town"), StringLength(32), MaxLength(32)]
        public string RepresentativeCity { get; set; }

        [Display(Name = "State"), StringLength(32), MaxLength(32)]
        public string RepresentativeState { get; set; }

        [Display(Name = "Zip Code"), StringLength(16), MaxLength(16)]
        public string RepresentativeZipCode { get; set; }

        //Reverification or Rehires
        [Display(Name = "Last Name (Family Name)"), StringLength(32), MaxLength(32)]
        public string RehireLastName { get; set; }

        [Display(Name = "First Name (Given Name)"), StringLength(32), MaxLength(32)]
        public string RehireFirstName { get; set; }

        [Display(Name = "Middle Initial"), StringLength(16), MaxLength(16)]
        public string RehireMiddleInitial { get; set; }

        [Display(Name = "Rehire Date")]
        public DateTime? RehireDate { get; set; }

        [Display(Name = "Document Title"), StringLength(32), MaxLength(32)]
        public string DocumentTitle { get; set; }

        [Display(Name = "Document Number"), StringLength(32), MaxLength(32)]
        public string DocumentNumber { get; set; }

        [Display(Name = "Document Expiry Date")]
        public DateTime? DocumentExpiryDate { get; set; }

        [Display(Name = "Employer Signature Date")]
        public DateTime? EmployerSignatureDate { get; set; }

        [Display(Name = "Employer Print Name"), StringLength(32), MaxLength(32)]
        public string EmployerPrintName { get; set; }        
    }


    public static class I9FormExtensions
    {
        public static I9FormUSVM ToViewModel(this I9FormUS model)
        {
            var viewModel = new I9FormUSVM();

            //For I9Form
            Utils.Map(model, viewModel);

            //For Accep Doc 
            //viewModel.DocumentsA = MapDocToViewModel(model.DocumentsA);

            return viewModel;
        }

        public static I9FormUS ToModel(this I9FormUSVM viewModel, I9FormUS model = null)
        {
            if (model == null) model = new I9FormUS();
            
            //For I9Form
            Utils.Map(viewModel, model);
            
            //For Accep Doc
            //model.DocumentsA = MapDocToModel(viewModel.DocumentsA);

            return model;
        }

    }
}


