﻿using OS.EMS.Model;

namespace OS.EMS.Security
{
    public interface ILoginService
    {
        SystemUser User { get; }
        void SignIn(SystemUser user);
        void SignOut();
    } 
}
