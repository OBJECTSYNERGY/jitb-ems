using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace OS.EMS.Services.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveDiacritics(this string text)
        {
            if (text == null)
                return null;

            var sb = new StringBuilder();

            foreach (char c in text.Normalize(NormalizationForm.FormD))
            {
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    sb.Append(c);
            }
            return sb.ToString();
        }

        public static string ToUrlFriendly(this string text)
        {
            return Regex.Replace(text.Trim().ToLower(), @"[^a-z0-9]+", "-");
        }
    }
}