﻿using System;

namespace OS.EMS.Model
{
    public interface IAuditEntity : IEntity
    {
        DateTime? CreatedOn { get; set; }
        DateTime? ModifiedOn { get; set; }
        DateTime? DeletedOn { get; set; }

        Guid? CreatedById { get; set; }
        Guid? ModifiedById { get; set; }
        Guid? DeletedById { get; set; }

        SystemUser CreatedBy { get; set; }
        SystemUser ModifiedBy { get; set; }
        SystemUser DeletedBy { get; set; }
    }


    public class AuditEntity : Entity, IAuditEntity
    {
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        
        public Guid? CreatedById { get; set; }
        public Guid? ModifiedById { get; set; }
        public Guid? DeletedById { get; set; }

        public virtual SystemUser CreatedBy { get; set; }
        public virtual SystemUser ModifiedBy { get; set; }
        public virtual SystemUser DeletedBy { get; set; }
    }
}
