﻿using OS.EMS.Model;

namespace OS.EMS.Data
{
    public interface IAppUow
    {        
        // Save pending changes to the data store.
        void Commit();

        // Repositories
        IRepository<AppCode> AppCodes { get; }
        IRepository<Country> Countries { get; }
        IRepository<State> States { get; }
        IRepository<City> Cities { get; }

        IRepository<Role> Roles { get; }
        IRepository<EmployeeDesignation> EmployeeDesignation { get; }
        
        //IRepository<Role> Roles { get; }

        ISystemUsersRepository SystemUsers { get; }
        
        IRepository<Applicant> Applicants { get; }
        IRepository<ApplicantInterview> ApplicantInterviews { get; }
        IRepository<EducationType> EducationTypes { get; }
        IRepository<ApplicantEducation> ApplicantEducations { get; }
        IRepository<ApplicantWorkHistory> ApplicantWorkHistories { get; }

        IEmployeeRepository Employees { get; }
        IRepository<I9FormUS> I9FormsUS { get; }
        IRepository<Attendance> Attendances { get; }
        IRepository<AttendanceData> AttendancesData { get; }
        IRepository<AcceptableDocument> AcceptableDocuments { get; }

        IRepository<FormW4> FormW4 { get; }
        

        IStoresRepository Stores { get; }

    }
}
