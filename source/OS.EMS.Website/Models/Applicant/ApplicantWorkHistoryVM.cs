﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Services;

namespace OS.EMS.Web.Models
{
    public class ApplicantWorkHistoryVM
    {
         public ApplicantWorkHistoryVM() {
            
        }

        [Inject]
         public ApplicantWorkHistoryVM(IGeographyService geographyService)
        {

            //var states = geographyService.GetStates();
            //States = states.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = s.Name });

            //var cities = geographyService.GetCities();
            //Cities = cities.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });


        }


        public Guid Id { get; set; }
        public Guid ApplicantId { get; set; }

        [StringLength(32), MaxLength(32)]
        public string EmployerName { get; set; }

        [StringLength(128), MaxLength(128)]
        public string Address { get; set; }

        //[Required(ErrorMessage = "Please specify City!")]
        public Guid? CityId { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }

        //[Required(ErrorMessage = "Please specify State!")]
        public Guid? StateId { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }

        //[Required(ErrorMessage = "Please specify ZipCode!")]
        [StringLength(16), MaxLength(16)]
        public string ZipCode { get; set; }

        //[Display(Name = "Phone"), Required(ErrorMessage = "Please specify Phone!")]
        [StringLength(16), MaxLength(16)]
        public string Phone { get; set; }

        public DateTime? WorkedFrom { get; set; }
        public DateTime? WorkedTo { get; set; }

        [StringLength(64), MaxLength(64)]
        public string  Position { get; set; }

        [StringLength(32), MaxLength(32)]
        public string WorkNature { get; set; }

        [StringLength(32), MaxLength(32)]
        public string Supervisor { get; set; }

        public decimal HourlyPayAtStart { get; set; }
        public decimal HourlyPayAtPresent { get; set; }

        [StringLength(256), MaxLength(256)]
        public string ReasonLeaving { get; set; }

        public int IntCounter { get; set; }


    }
}