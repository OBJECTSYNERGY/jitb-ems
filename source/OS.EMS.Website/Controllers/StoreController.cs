﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Services;
using OS.EMS.Model.Helper;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{
    public class StoreController : BaseController
    {
        [Inject]
         public StoreController(IStoreService storeService, IEmployeeService employeeService, IGeographyService geographyService, ISecurityService securityService)
        {
            StoreService = storeService;
            EmployeeService = employeeService;
            GeographyService = geographyService;
            SecurityService = securityService;
        }

        #region CRUD Operations

        public ActionResult Create()
        {
            var viewModel = new StoreVM();
            PopulateGeographySLI(viewModel);

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Create(StoreVM viewModel)
        {
            if (viewModel == null) return RedirectToAction("InvalidRequest", "Error");

            var store = viewModel.ToModel();

            if (StoreService.Create(store))
            {
                ShowMessage(Messages.CreateStoreSuccess, MessageType.Success);
                return RedirectToAction("Details", new { id = store.Id });
            }
            return RedirectToAction("InvalidRequest", "Error");
        }

        public ActionResult Update(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var store = StoreService.Get(id.Value);
            var viewModel = store.ToStoreVM();
            PopulateGeographySLI(viewModel);
            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Update(StoreVM viewModel)
        {
            if (viewModel == null || viewModel.Id == null) return RedirectToAction("InvalidRequest", "Error");

            var store = viewModel.ToModel();
            if (StoreService.Update(store))
            {
                ShowMessage(Messages.UpdateStoreSuccess, MessageType.Success);
                return RedirectToAction("Details", new{ id=store.Id});
            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult List()
        {
            var user = HttpContext.Session["AppUser"] as SystemUser;
            if (user == null) return RedirectToAction("InvalidRequest", "Error");

            var role = SecurityService.GetRolesById(user.RoleId.Value);

            IEnumerable<Store> stores = null;
            if (user.EmployeeId.HasValue && user.Role.Name == RoleType.Manager.ToString())
            {
                stores = StoreService.GetByManagerId(user.EmployeeId.Value);
            }
            else if (user.Role.Name == RoleType.Admin.ToString())
            {
                stores = StoreService.GetAll();
            }
            else{
                return RedirectToAction("InvalidRequest", "Error");
            }

            
            //var stores = (role.Name == RoleType.Manager.ToString()) ? StoreService.GetByManagerId(user.EmployeeId.Value) : StoreService.GetAll();
            var storesVM = stores.ToStoreVM();
            return View(storesVM);
            
        }

        public ActionResult Activate(Guid? id, bool active)
        {
            if (!id.HasValue) return RedirectToAction("InvalidRequest", "Error");

            if (StoreService.Activate(id.Value, active))
            {
                ShowMessage(active ? Messages.ActivateStoreSuccess : Messages.DeactivateStoreSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult SearchByCode(string StoreCode)
        {
            if (string.IsNullOrEmpty(StoreCode))
            {
                return RedirectToAction("Dashboard", "Home");
            }

            var stores = StoreService.SearchByCode(StoreCode);
            var storesVM = stores.ToStoreVM();

            if (storesVM.Count() == 1)
            {
                var viewModel = storesVM.FirstOrDefault();
                PopulateGeographySLI(viewModel);

                ////For Employees of Store
                //if (viewModel != null && viewModel.Id.HasValue)
                //{
                //    ForStoreEmployees(viewModel.Id.Value);
                //}

                return View("Details", viewModel);
            }
            else if (storesVM.Count() > 1)
            {
                return View("List", storesVM);
            }

            return RedirectToAction("Dashboard", "Home");
        }

        public ActionResult Details(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var store = StoreService.Get(id.Value);
            var viewModel = store.ToStoreVM();

            ////For Employees of Store
            //if (store != null)
            //{
            //    ForStoreEmployees(store.Id);
            //}
            return View(viewModel);
        }

        public ActionResult SetSupervisor(Guid id, Guid eid, Guid? peid)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            if(StoreService.SetSupervisor(id, eid, peid)){
                ShowMessage(Messages.AssignSupervisorStoreSuccess, MessageType.Success);
                return RedirectToAction("Details", new { id = id});
            }

            return RedirectToAction("List");

        }

        [HttpGet]
        public virtual JsonResult IsEmailExists(string Email, string InitialEmail)
        {
            //Thread.Sleep(2000);//fake delay
            if (Email == InitialEmail)
            {
                return this.Json(true, JsonRequestBehavior.AllowGet);
            }

            return this.Json(!StoreService.IsEmailExists(Email), JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region AppHelper

        private void PopulateGeographySLI(StoreVM viewModel)
        {
            var states = GeographyService.GetStates();
            viewModel.States = states.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = s.Name });

            var cities = GeographyService.GetCities();
            viewModel.Cities = cities.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
        }

        #endregion

    }
}
