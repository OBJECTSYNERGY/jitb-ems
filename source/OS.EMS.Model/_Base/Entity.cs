using System;

namespace OS.EMS.Model
{
    public interface IEntity
    {
        Guid Id { get; set; }
        bool IsActive { get; set; }
    }

    public class Entity : IEntity
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
    }
}