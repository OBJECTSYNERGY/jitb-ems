using System;
using System.ComponentModel.DataAnnotations;

namespace OS.EMS.Model
{
    public class State : Entity
    {
        [StringLength(32)]
        public string Code { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        public Guid? CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}