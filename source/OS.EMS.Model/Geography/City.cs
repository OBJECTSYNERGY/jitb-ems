using System;
using System.ComponentModel.DataAnnotations;

namespace OS.EMS.Model
{
    public class City : Entity
    {
        [StringLength(32)]
        public string Name { get; set; }

        [StringLength(32)]
        public string Code { get; set; }

        public Guid? StateId { get; set; }
        public Guid? CountryId { get; set; }

        public virtual State State { get; set; }
        public virtual Country Country { get; set; }
    }
}