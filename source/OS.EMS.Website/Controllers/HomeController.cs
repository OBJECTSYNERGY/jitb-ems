﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using Ninject.Extensions.Logging;
using OS.EMS.Services;
using OS.EMS.Model.Helper;
using OS.EMS.Web.Models;


namespace OS.EMS.Web.Controllers
{
    public class HomeController : BaseController
    {


        [Inject]
        public HomeController(IStoreService storeService, IApplicantService applicantService, IEmployeeService employeeService, SecurityService securityService)
        {
            ApplicantService = applicantService;
            EmployeeService = employeeService;
            StoreService = storeService;
            SecurityService = securityService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Dashboard()
        {
            var applicants = ApplicantService.GetByActivatedType(false);

            DashboardVM modelVM = new DashboardVM();
            
            var applicantsVM = new List<ApplicantVM>();
            Utils.Map(applicants, applicantsVM);
            //ViewData["ApplicantsVM"] = applicantsVM;
            modelVM.PendingApplicants = applicantsVM;

            //For Quick Stats
            modelVM.PendingApplicantsCount = ApplicantService.PendingApplicantsCount(false);
            modelVM.ActiveEmployeeCount =  EmployeeService.TotalEmployeesCount(true);
            modelVM.ActiveStoresCount = StoreService.TotalStoresCount(true);
            modelVM.SystemUsersCount = SecurityService.TotalSystemUsersCount(true);

            //For Store Listing
            var stores = StoreService.GetAll();
            //ViewData["StoresSLI"] = stores.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });
            modelVM.Stores = stores.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });


            return View(modelVM);
        }

        
        public ActionResult TestPage()
        {
            return View();
        }

    }
}
