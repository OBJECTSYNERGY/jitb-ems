﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Web.Models
{
    public class AttendanceVM
    {
        
        public Guid? Id { get; set; }
        public bool IsActive { get; set; }
        public EmployeeVM Employee { get; set; }

        [StringLength(32), MaxLength(32)]
        public string AcountNo { get; set; }

        [StringLength(32), MaxLength(32)]
        public string Name { get; set; }

        public DateTime Time { get; set; }

        [StringLength(32), MaxLength(32)]
        public string State { get; set; }

        public string Exception { get; set; }

        
    }

    public static class AttendanceVMExtensions
    {
        public static AttendanceVM ToModelVM(this Attendance model)
        {
            var viewModel = new AttendanceVM();
            Utils.Map(model, viewModel);
            return viewModel;
        }

        public static Attendance ToModel(this AttendanceVM viewModel, Attendance store = null)
        {
            if (store == null) store = new Attendance();
            Utils.Map(viewModel, store);
            return store;
        }

    }

}