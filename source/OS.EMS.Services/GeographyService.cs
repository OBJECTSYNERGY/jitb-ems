﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    public interface IGeographyService : IBusinessService
    {
        IEnumerable<Country> GetCountries();
        IEnumerable<State> GetStates(Guid? countryId = null);
        IEnumerable<City> GetCities(Guid? stateId = null);
    }

    public class GeographyService : IGeographyService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }
        
        public GeographyService(IAppUow uow)
        {
            Uow = uow;
        }

        public IEnumerable<Country> GetCountries()
        {
            return Uow.Countries.GetAll();
        }

        public IEnumerable<State> GetStates(Guid? countryId = null)
        {
            return Uow.States.GetAll();
        }

        public IEnumerable<City> GetCities(Guid? stateId = null)
        {
            if(stateId == null) return Uow.Cities.GetAll();

            return Uow.Cities.GetAll().Where(c => c.StateId == stateId.Value);
        }        
    } 
}
