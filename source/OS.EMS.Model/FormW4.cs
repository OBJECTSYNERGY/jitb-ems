﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OS.EMS.Model
{
    public class FormW4:Entity
    {
        //public FormW4(){}

        public Guid? EmployeeId { get; set; }
        //public virtual Employee Employee { get; set; }

        //For Personal Allownance Worksheet
        public string Allowance_A { get; set; }
        public string Allowance_B { get; set; }
        public string Allowance_C { get; set; }
        public string Allowance_D { get; set; }
        public string Allowance_E { get; set; }
        public string Allowance_F { get; set; }
        public string Allowance_G { get; set; }
        public string Allowance_H { get; set; }

        //For Employee's Withholding Allowance Certificate
        public string MartialStatusType { get; set; }

        public int TotalAllowancesClaming_5 { get; set; }
        public int AdditionalAmtWithheld_6 { get; set; }
        public string Allowance_7 { get; set; }

        //Deductions and Adjustments
        public int DedAndAdj_1 { get; set; }
        public int DedAndAdj_2 { get; set; }
        public int DedAndAdj_3 { get; set; }
        public int DedAndAdj_4 { get; set; }
        public int DedAndAdj_5 { get; set; }
        public int DedAndAdj_6 { get; set; }
        public int DedAndAdj_7 { get; set; }
        public int DedAndAdj_8 { get; set; }
        public int DedAndAdj_9 { get; set; }
        public int DedAndAdj_10 { get; set; }

        //Deductions and Adjustments -  (Two-Earners/Multiple Jobs Worksheet)


        public int DedAndAdj_TwoEarners_1 { get; set; }
        public int DedAndAdj_TwoEarners_2 { get; set; }
        public int DedAndAdj_TwoEarners_3 { get; set; }
        public int DedAndAdj_TwoEarners_4 { get; set; }
        public int DedAndAdj_TwoEarners_5 { get; set; }
        public int DedAndAdj_TwoEarners_6 { get; set; }
        public int DedAndAdj_TwoEarners_7 { get; set; }
        public int DedAndAdj_TwoEarners_8 { get; set; }
        public int DedAndAdj_TwoEarners_9 { get; set; }


    }
}
