﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    public interface ISecurityService : IBusinessService
    {

        bool Create(SystemUser user);
        bool Update(SystemUser user);
        IEnumerable<SystemUser> GetAll();
        SystemUser Get(Guid id);
        Role GetRolesById(Guid id);
        bool Activate(Guid id, bool active);

        IEnumerable<Role> GetRoles();
        SystemUser Authenticate(string userName, string password);

        int TotalSystemUsersCount(bool isActive);
        bool IsEmailExists(string emailAddress);
    }

    public class SecurityService : ISecurityService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        //For other Services
        private IEmployeeService EmployeeService { get; set; }

        public SecurityService(IAppUow uow)
        {
            Uow = uow;
        }


        public bool Create(SystemUser user)
        {
            try
            {
                //Default Mode
                user.IsActive = true;

                Uow.SystemUsers.Add(user);
                Uow.Commit();

                //For Employee Status As System User
                if (user.EmployeeId.HasValue)
                {
                    var employee = Uow.Employees.GetById(user.EmployeeId.Value);
                    employee.SystemUserId = user.Id;
                    Uow.Employees.Update(employee);
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create System User");
                return false;
            }
        }

        public bool Update(SystemUser user)
        {
            try
            {
                Uow.SystemUsers.Update(user);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update System User");
                return false;
            }
        }

        public SystemUser Get(Guid id)
        {
            return Uow.SystemUsers.GetById(id);
        }

        public IEnumerable<SystemUser> GetAll()
        {
            return Uow.SystemUsers.GetAll();
        }

        public bool Activate(Guid id, bool active)
        {
            try
            {
                var user = Get(id);
                user.IsActive = active;
                Uow.SystemUsers.Update(user);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Activate System User");
                return false;
            }     
        }

        public SystemUser Authenticate(string email, string password)
        {
            return Uow.SystemUsers.GetAll()
                    .SingleOrDefault(u => u.Email == email && u.Password == password && u.IsActive );
        }

        public IEnumerable<Role> GetRoles()
        {
            return Uow.Roles.GetAll();
        }

        public Role GetRolesById(Guid id)
        {
            return Uow.Roles.GetAll().FirstOrDefault(x => x.Id == id);
        }

        public int TotalSystemUsersCount(bool isActive)
        {
            return Uow.SystemUsers.GetAll().Count(x => x.IsActive == isActive);
        }

        public bool IsEmailExists(string emailAddress)
        {
            var models = Uow.SystemUsers.GetAll().Where(s => s.Email == emailAddress);
            return models.Any() ? true : false;
        }

    } 
}
