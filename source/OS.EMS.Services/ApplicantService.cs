﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Services
{
    public interface IApplicantService : IBusinessService
    {
        bool Create(Applicant applicant);
        bool Update(Applicant applicant);
        IEnumerable<Applicant> GetAll();
        IEnumerable<Applicant> GetByActivatedType(bool isActive);
        bool Activate(Guid id, bool active);
        Applicant Get(Guid id);
        Applicant Get(string code);

        Applicant GetLast();

        int PendingApplicantsCount(bool isActive);


        IEnumerable<EducationType> GetEducationType();

        IEnumerable<Applicant> SearchByCode(string searchCode);
        IEnumerable<Applicant> SearchByname(string searchName);

        bool IsEmailExists(string emailAddress);

    }

    public class ApplicantService : IApplicantService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        public ApplicantService(IAppUow uow, ILogger logger)
        {
            Uow = uow;
            Logger = logger;
        }

        public bool Create(Applicant applicant)
        {
            try
            {
                //For Applicant Code Durign Create
                var lastApplicant = GetLast();
                if (lastApplicant != null)
                {
                    int applicantSufix = int.Parse(lastApplicant.Code.Substring(AppConstants.ApplicantPrefix.Length));
                    applicant.Code = string.Format("{0}{1}", AppConstants.ApplicantPrefix, ++applicantSufix);
                }
                else
                {
                    int applicationSeed = int.Parse(AppConstants.ApplicationSeed);
                    applicant.Code = string.Format("{0}{1}", AppConstants.ApplicantPrefix, ++applicationSeed);
                }

                applicant.IsActive = false; 

                //For Create Command
                Uow.Applicants.Add(applicant);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create applicant");
                return false;
            }            
        }

        public bool Update(Applicant applicant)
        {
            try
            {
                Uow.Applicants.Update(applicant);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update applicant");
                return false;
            }     
        }

        public IEnumerable<Applicant> GetAll()
        {
            return Uow.Applicants.GetAll();
        }

        public IEnumerable<Applicant> GetByActivatedType(bool isActive)
        {
            return Uow.Applicants.GetAll().Where(x => x.IsActive == isActive);
        }

        public bool Activate(Guid id, bool active)
        {
            try
            {
                var applicant = Get(id);
                applicant.IsActive = active;
                Uow.Applicants.Update(applicant);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Activate Application applicant");
                return false;
            }     
        }

        public Applicant Get(Guid Id)
        {
            var applicant = Uow.Applicants.GetById(Id);
            return applicant;
        }

        public Applicant Get(string code)
        {
            return Uow.Applicants.GetAll()
                    .SingleOrDefault(s => s.Code == code);
        }

        public Applicant GetLast()
        {

            var applicants = Uow.Applicants.GetAll().OrderByDescending(x => x.CreatedOn);
            if (applicants.Any())
            {
                return applicants.First();
            }

            return null;

        }


        public IEnumerable<EducationType> GetEducationType()
        {
            return Uow.EducationTypes.GetAll();
        }

        public IEnumerable<Applicant> SearchByCode(string searchCode)
        {
            return Uow.Applicants.GetAll().Where(x => x.Code.ToLower().Contains(searchCode));
        }

        public IEnumerable<Applicant> SearchByname(string searchName)
        {
            
            return Uow.Applicants.GetAll().Where(x => (x.LastName + " " + x.FirstName).ToLower().Contains(searchName));

        }

        public int PendingApplicantsCount(bool isActive)
        {
            return Uow.Applicants.GetAll().Count(x => x.IsActive == isActive);
        }

        public bool IsEmailExists(string emailAddress)
        {
            var models = Uow.Applicants.GetAll().Where(s => s.Email == emailAddress);
            return models.Any() ? true : false;
        }
        
    } 
}
