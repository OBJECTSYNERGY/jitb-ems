﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{

    public interface IAppCodeServiec : IBusinessService
    {
        bool Create(AppCode appCode);
        bool Update(AppCode appCode);
        IEnumerable<AppCode> GetAll();
        AppCode Get(Guid id);

        
    }

    public class AppCodeServiec : IAppCodeServiec
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        public AppCodeServiec(IAppUow uow)
        {
            Uow = uow;
        }

        public bool Create(AppCode appCode)
        {
            try
            {
                Uow.AppCodes.Add(appCode);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create AppCode");
                return false;
            }
        }

        public bool Update(AppCode appCode)
        {
            try
            {
                Uow.AppCodes.Update(appCode);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update AppCode");
                return false;
            }
        }

        public IEnumerable<AppCode> GetAll()
        {
            return Uow.AppCodes.GetAll();
        }

        public AppCode Get(Guid Id)
        {
            return Uow.AppCodes.GetById(Id);
        }
      
    } 
}
