﻿using System.ComponentModel.DataAnnotations;

namespace OS.EMS.Model
{
    public class EducationType : Entity
    {
        [StringLength(32)]
        public string Code { get; set; }

        [StringLength(32)]
        public string Title { get; set; }
    }


}
