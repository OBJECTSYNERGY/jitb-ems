﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using OS.EMS.Model;
using OS.EMS.Model.Helper;


namespace OS.EMS.Web.Models
{
    public class EmployeeVM
    {
        public EmployeeVM()
        {
            AttendanceInfo = new AttendanceInfoVM();
        }

        public Guid? Id { get; set; }

        [Required(ErrorMessage = "Please specify Employee Code!"), StringLength(32), MaxLength(32)]
        public string Code { get; set; }

        [StringLength(32), MaxLength(128)]
        public string AttendanceCode { get; set; }

        public bool IsManager { get; set; }

        //public bool HasI9Form { get; set; }

        public AttendanceInfoVM AttendanceInfo { get; set; } 

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        public Guid? ApplicantId { get; set; }
        public Applicant Applicant { get; set; }

        public Guid? SystemUserId { get; set; }
        public SystemUser SystemUser { get; set; }

        public Guid? I9FormId { get; set; }
        //public I9FormUS I9FormUS { get; set; }

        public Guid? FormW4Id { get; set; }

        [Required(ErrorMessage = "Please specify Store!")]
        public Guid? StoreId { get; set; }

        public Store Store { get; set; }
        public IEnumerable<SelectListItem> Stores { get; set; }
        

        //FOR PERSONAL 
        [Display(Name = "Last Name"), Required(ErrorMessage = "Please specify Last Name!"), StringLength(32), MaxLength(32)]
        public string LastName { get; set; }

        [Display(Name = "First Name"),Required(ErrorMessage = "Please specify First Name!"), StringLength(32), MaxLength(32)]
        public string FirstName { get; set; }

        [Display(Name = "Middle Ini"), StringLength(16), MaxLength(16)]
        public string MiddleInitial { get; set; }
        
        [Display(Name = "Street Address"),Required(ErrorMessage = "Please specify Street Address!"), StringLength(128), MaxLength(128)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please specify City!")]
        public Guid? CityId { get; set; }
        public City City { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }

        [Required(ErrorMessage = "Please specify State!")]
        public Guid? StateId { get; set; }
        public State State { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }

        [Required(ErrorMessage = "Please specify ZipCode!"),StringLength(16), MaxLength(16)]
        public string ZipCode { get; set; }

        [Display(Name = "Home Phone"),Required(ErrorMessage = "Please specify Phone Number!"), RegularExpression("^[0-9()-]+$", ErrorMessage = "Phone Number should be numberic!"), StringLength(16), MaxLength(16)]
        public string Phone { get; set; }

        [Display(Name = "Email Address"),Required(ErrorMessage = "Please specify Email Address!"), StringLength(128), MaxLength(128)]
        [Remote("IsEmailExists", "Employee", ErrorMessage = "Email Address is not available", AdditionalFields = "InitialEmail")]
        public string Email { get; set; }

        [Display(Name = "Date of Birth"),Required(ErrorMessage = "Please specify Date of Birth!")]
        public DateTime DateOfBirth { get; set; }
        
        //For Salery
        [Required(ErrorMessage = "Please specify Salery Rate!"), RegularExpression("^[0-9.]+$", ErrorMessage = "Salery Rat should be numberic!")]
        public double SaleryRate { get; set; }

        public double WorkingHours { get; set; }

        [StringLength(32)]
        public string SocialSecurityNumber { get; set; }

        public bool LastNameDiffer { get; set; }
    }


    public static class EmployeeExtensions
    {
        public static EmployeeVM ToEmployeeVM(this Employee source)
        {
            var destination = new EmployeeVM();
            Utils.Map(source, destination);            
            return destination;
        }

        public static List<EmployeeVM> ToEmployeeVM(this IEnumerable<Employee> model)
        {
            var viewModel = new List<EmployeeVM>();
            Utils.Map(model, viewModel);
            return viewModel;
        }

        public static Employee ToModel(this EmployeeVM source, Employee destination = null)
        {
            if (destination == null) destination = new Employee();
            Utils.Map(source, destination);
            return destination;
        }

    }

}