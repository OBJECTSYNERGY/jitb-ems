﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OS.EMS.Model
{
    public class Attendance : Entity
    {

        public Guid? EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        [StringLength(32)]
        public string AcountNo { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        public DateTime Time { get; set; }

        [StringLength(32)]
        public string State { get; set; }

        public string Exception { get; set; }



    }
}
