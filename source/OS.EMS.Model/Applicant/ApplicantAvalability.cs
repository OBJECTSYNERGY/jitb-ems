using System;

namespace OS.EMS.Model
{
    public class ApplicantAvalability : AuditEntity
    {
        public Guid ApplicantId { get; set; }

        //[ForeignKey("ApplicantId")]
        public virtual Applicant Applicant { get; set; }

        public int Day { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}