using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OS.EMS.Model
{
    public class Employee : AuditEntity
    {
        [StringLength(32)]
        public string Code { get; set; }

        [StringLength(32)]
        public string AttendanceCode { get; set; }

        public Guid? ApplicantId { get; set; }
        public virtual Applicant Applicant { get; set; }

        public Guid? StoreId { get; set; }

        //If UnCommit System Will Generate auto id
        //[ForeignKey("StoreId")] 
        public virtual Store Store { get; set; }

        public Guid? SystemUserId { get; set; }
        public virtual SystemUser SystemUser { get; set; }

        public bool IsManager { get; set; }
        
        //public bool HasI9Form{ get; set; }
        public Guid? I9FormId { get; set; }

        [ForeignKey("I9FormId")] 
        public virtual I9FormUS I9FormUS { get; set; }

        public Guid? FormW4Id { get; set; }

        [ForeignKey("FormW4Id")] 
        public virtual FormW4 FormW4 { get; set; }

        [StringLength(32)]
        public string LastName { get; set; }

        [StringLength(32)]
        public string FirstName { get; set; }

        [StringLength(16)]
        public string MiddleInitial { get; set; }

        [StringLength(128)]
        public string Address { get; set; }

        public Guid? CityId { get; set; }
        public virtual City City { get; set; }

        public Guid? StateId { get; set; }
        public virtual State State { get; set; }

        public Guid? CountryId { get; set; }
        public virtual Country Country { get; set; }

        [StringLength(16)]
        public string ZipCode { get; set; }

        [StringLength(16)]
        public string Phone { get; set; }

        [StringLength(64)]
        public string Email { get; set; }
        
        public DateTime? DateOfBirth { get; set; }

        //For Salery
        public double SaleryRate { get; set; }
        //public int WorkingHours { get; set; }

        public string SocialSecurityNumber { get; set; }
        public bool LastNameDiffer { get; set; }
        
    }
}