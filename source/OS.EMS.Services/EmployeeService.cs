﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Services
{
    public interface IEmployeeService : IBusinessService
    {
        bool Create(Employee employee);
        bool Update(Employee employee);
        IEnumerable<Employee> GetAll();
        bool Activate(Guid id, bool active);
        Employee Get(Guid id);
        Employee Get(string code);

        IEnumerable<Employee> GetByStoreId(Guid id);
        IEnumerable<Employee> GetByApplicantId(Guid id);
        IEnumerable<Employee> GetManagers(bool isManager);
        IEnumerable<Employee> SearchByCode(string employeeCode);
        IEnumerable<Employee> SearchByCode(string employeeCode, Guid storeId);
        IEnumerable<Employee> SearchByName(string employeeName);
        IEnumerable<Employee> SearchByName(string employeeName, Guid storeId);


        int TotalEmployeesCount(bool isActive);

        bool IsEmailExists(string emailAddress);

        bool AssignStore(Guid applicantId, Guid storeId, out Guid employeeId);

    }

    public class EmployeeService : IEmployeeService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        protected IApplicantService ApplicantService { get; set; }

        public EmployeeService(IAppUow uow, ILogger logger, IApplicantService applicantService)
        {
            Uow = uow;
            Logger = logger;
            ApplicantService = applicantService;
        }

        public bool Create(Employee employee)
        {
            try
            {
                //Make it Active
                employee.IsActive = true;
                employee.IsManager = false;

                Uow.Employees.Add(employee);
                Uow.Commit();

                //For Applicant Status Updating to active
                var applicant = ApplicantService.Get(employee.ApplicantId.Value);
                applicant.IsActive = true;
                ApplicantService.Update(applicant);
                Uow.Commit();

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create Employee");
                return false;
            }            
        }

        public bool Update(Employee employee)
        {
            try
            {
                Uow.Employees.Update(employee);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update Employee");
                return false;
            }     
        }

        public IEnumerable<Employee> GetAll()
        {
            return Uow.Employees.GetAll();
        }

        public bool Activate(Guid id, bool active)
        {
            try
            {
                var employee = Get(id);
                employee.IsActive = active;
                Uow.Employees.Update(employee);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Activate Employee");
                return false;
            }     
        }

        public Employee Get(Guid id)
        {
            return Uow.Employees.GetById(id);
        }

        public Employee Get(string code)
        {
            return Uow.Employees.GetAll()
                    .SingleOrDefault(s => s.Code == code);
        }

        public IEnumerable<Employee> GetByStoreId(Guid id)
        {
            return Uow.Employees.GetAll().Where(x => x.StoreId == id);
        }

        public IEnumerable<Employee> GetByApplicantId(Guid id)
        {
            return Uow.Employees.GetAll().Where(x => x.ApplicantId == id);
        }

        public IEnumerable<Employee> GetManagers(bool isManager)
        {
            return Uow.Employees.GetAll().Where(x => x.IsManager == isManager);
        }

        public IEnumerable<Employee> SearchByCode(string employeeCode)
        {
            return Uow.Employees.GetAll().Where(x => x.Code.ToLower().Contains(employeeCode.ToLower()));
        }

        public IEnumerable<Employee> SearchByCode(string employeeCode, Guid storeId)
        {
            return Uow.Employees.GetAll().Where(x => x.StoreId == storeId && (x.Code.ToLower().Contains(employeeCode.ToLower())));
        }

        public IEnumerable<Employee> SearchByName(string employeeName)
        {
            return Uow.Employees.GetAll().Where(x => (x.LastName + " " + x.FirstName).ToLower().Contains(employeeName.ToLower()));
        }


        public IEnumerable<Employee> SearchByName(string employeeName, Guid storeId)
        {

            return Uow.Employees.GetAll().Where(x => x.StoreId == storeId && ((x.LastName + " " + x.FirstName).ToLower().Contains(employeeName.ToLower())));
        }

        public int TotalEmployeesCount(bool isActive)
        {
            return Uow.Employees.GetAll().Count(x => x.IsActive == isActive);
        }


        public bool IsEmailExists(string emailAddress)
        {
            var models = Uow.Employees.GetAll().Where(s => s.Email == emailAddress);
            return models.Any() ? true : false;
        }

        public bool AssignStore(Guid applicantId, Guid storeId, out Guid employeeId)
        {
            bool status = false;
            var applicant = ApplicantService.Get(applicantId);

            //For New Employee from applicant info
            var employee = new Employee();
            employee.ApplicantId = applicant.Id;
            employee.StoreId = storeId;

            //Make it Active
            employee.IsActive = true;

            //For EmployeeCode from Applicant 
            int applicantSufix = int.Parse(applicant.Code.Substring(AppConstants.ApplicantPrefix.Length));
            employee.Code = string.Format("{0}{1}", AppConstants.EmployeePrefix, applicantSufix);

            //For Applicant to Employee mapping
            employee.LastName = applicant.LastName;
            employee.FirstName = applicant.FirstName;
            employee.MiddleInitial = applicant.MiddleInitial;
            employee.Address = applicant.Address;

            employee.CityId = applicant.CityId;
            employee.StateId = applicant.StateId;

            employee.ZipCode = applicant.ZipCode;
            employee.Phone = applicant.Phone;
            employee.Email = applicant.Email;

            employee.DateOfBirth = applicant.DateOfBirth;

            //For Applicant Status, ConfirmationMsg & Rediraction
            if (Create(employee))
            {
                //Update Appliacnt Status
                applicant.IsActive = true;
                if (ApplicantService.Update(applicant))
                {
                    //For Out
                    employeeId = employee.Id;
                    status = true;
                }
            }

            employeeId = employee.Id;
            return status;
        }
    } 
}
