﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    public interface IAttendanceService : IBusinessService
    {
        bool Create(Attendance attendance);
        bool Update(Attendance attendance);
        IEnumerable<Attendance> GetAll();
        bool Activate(Guid id, bool active);
        Attendance Get(Guid id);
        IEnumerable<Attendance> GetByEmployeeId(Guid id);
        //Attendance GetLast();
    }

    public class AttendanceService : IAttendanceService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        public AttendanceService(IAppUow uow)
        {
            Uow = uow;
        }

        #region CRUD Operations

        public bool Create(Attendance attendance)
        {
            try
            {
                Uow.Attendances.Add(attendance);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create Attendance");
                return false;
            }            
        }

        public bool Update(Attendance attendance)
        {
            try
            {
                Uow.Attendances.Update(attendance);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update Attendance");
                return false;
            }     
        }

        public IEnumerable<Attendance> GetAll()
        {
            return Uow.Attendances.GetAll();
        }

        public IEnumerable<Attendance> GetByEmployeeId(Guid id)
        {
            return Uow.Attendances.GetAll().Where(x=>x.EmployeeId == id);
        }
       
        public bool Activate(Guid id, bool active)
        {
            try
            {
                var attendance = Get(id);
                attendance.IsActive = active;
                Uow.Attendances.Update(attendance);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Activate Application Attendance");
                return false;
            }     
        }

        public Attendance Get(Guid Id)
        {
            return Uow.Attendances.GetById(Id);
        }

        #endregion

    }
}
