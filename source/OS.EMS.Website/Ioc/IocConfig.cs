﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using Ninject.Extensions.Logging;
using Ninject.Extensions.Logging.Log4net;
using Ninject.Extensions.Conventions;
using OS.EMS;
using OS.EMS.Data;
using OS.EMS.Security;
using OS.EMS.Services;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using OS.EMS.Web.Security;

namespace OS.EMS.Web
{
    public class IocConfig
    {
        public static void RegisterIoc(HttpConfiguration config)
        {
            var settings = new NinjectSettings {LoadExtensions = false};

            var kernel = new StandardKernel(settings, new Log4NetModule()); // Ninject IoC

            //var kernel = new StandardKernel();

            // These registrations are "per instance request".
            // See http://blog.bobcravens.com/2010/03/ninject-life-cycle-management-or-scoping/

            //Database Repository Classes
            kernel.Bind<RepositoryFactories>().To<RepositoryFactories>().InSingletonScope();
            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();
            kernel.Bind<IAppUow>().To<AppUow>();
     
            //Api or General Classes
            kernel.Bind<IApplicationSettings>().To<ApplicationSettings>();
            kernel.Bind<PathFinder>().To<PathFinder>();

            kernel.Bind<ILoginService>().To<LoginService>();

            //var path = @"D:\Projects\EMS\root\source\OS.EMS.Website\bin";
            //var path = AppDomain.CurrentDomain.BaseDirectory + "bin";
            
            kernel.Bind(x =>
                //x.FromAssembliesInPath(path)
                x.FromAssembliesMatching("OS.EMS.*")
                .SelectAllClasses()
                .InheritedFrom<IBusinessService>()
                .BindDefaultInterfaces()
            );

            // Tell ControllerBuilder how to use Ninject IoC
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory(kernel));
            
            // Tell WebApi how to use our Ninject IoC
            //config.DependencyResolver = new NinjectDependencyResolver(kernel);

            ILoggerFactory loggerFactory = kernel.Get<ILoggerFactory>();
            ILogger logger = loggerFactory.GetCurrentClassLogger();

            logger.Debug("Application Started");

        }
    }
}