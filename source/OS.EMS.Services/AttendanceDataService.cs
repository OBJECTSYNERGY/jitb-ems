﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    public interface IAttendanceDataService : IBusinessService
    {
        bool Create(AttendanceData attendance);
        bool Update(AttendanceData attendance);
        IEnumerable<AttendanceData> GetAll();
        bool Activate(Guid id, bool active);
        AttendanceData Get(Guid id);
        IEnumerable<AttendanceData> GetByAcountNo(string id);
        IEnumerable<AttendanceData> GetByName(string name);
        IEnumerable<AttendanceData> GetByDate(string attendanceCode, DateTime fromDate, DateTime toDate);

    }

    public class AttendanceDataService : IAttendanceDataService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        public AttendanceDataService(IAppUow uow)
        {
            Uow = uow;
        }

        public bool Create(AttendanceData attendance)
        {
            try
            {
                Uow.AttendancesData.Add(attendance);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create Attendance Data");
                return false;
            }            
        }

        public bool Update(AttendanceData attendance)
        {
            try
            {
                Uow.AttendancesData.Update(attendance);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update Attendance Data");
                return false;
            }     
        }

        public IEnumerable<AttendanceData> GetAll()
        {
            return Uow.AttendancesData.GetAll();
        }
       
        public bool Activate(Guid id, bool active)
        {
            try
            {
                var attendance = Get(id);
                attendance.IsActive = active;
                Uow.AttendancesData.Update(attendance);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Activate Attendance Data");
                return false;
            }     
        }

        public AttendanceData Get(Guid id)
        {
            return Uow.AttendancesData.GetById(id);
        }


        public IEnumerable<AttendanceData> GetByAcountNo(string id)
        {
            return Uow.AttendancesData.GetAll().Where(x => x.AcountNo == id);
        }

        public IEnumerable<AttendanceData> GetByName(string name)
        {
            return Uow.AttendancesData.GetAll().Where(x => x.Name == name);
        }

        public IEnumerable<AttendanceData> GetByDate(string attendanceCode, DateTime fromDate, DateTime toDate)
        {
            //string parseCode = string.Empty;
            //switch (attendanceCode)
            //{
            //    case "JITB-EMP-9001":
            //        parseCode = "1";
            //        break;
            //    case "JITB-EMP-9002":
            //        parseCode = "2";
            //        break;
            //    case "JITB-EMP-9003":
            //        //19 	Syed Asim Akram 
            //        parseCode = "19";
            //        break;
            //    case "JITB-EMP-9004":
            //        //14 	Shahnawaz Qazi 
            //        parseCode = "14";
            //    break;
            //        case "JITB-EMP-9005":
            //        //36 	Syed Abdus Salam 
            //        parseCode = "36";
            //        break;
            //    default:
            //        parseCode = "19";
            //        break;
            //}

            return Uow.AttendancesData.GetAll().Where(x => x.Time >= fromDate && x.Time <= toDate && x.AcountNo == attendanceCode);
        }

    }
}
