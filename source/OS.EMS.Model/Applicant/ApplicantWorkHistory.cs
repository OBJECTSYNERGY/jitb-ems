﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OS.EMS.Model
{
    public class ApplicantWorkHistory : Entity
    {

        public Guid ApplicantId { get; set; }

        [ForeignKey("ApplicantId")]
        public virtual Applicant Applicant { get; set; }

        [StringLength(32)]
        public string EmployerName { get; set; }

        [StringLength(128)]
        public string Address { get; set; }

        public Guid? CityId { get; set; }
        public virtual City City { get; set; }

        public Guid? StateId { get; set; }
        public virtual State State { get; set; }
        
        //public int CountryId { get; set; }
        //public virtual Country Country { get; set; }

        [StringLength(16)]
        public string ZipCode { get; set; }

        [StringLength(16)]
        public string Phone { get; set; }

        public DateTime? WorkedFrom { get; set; }
        public DateTime? WorkedTo { get; set; }

        [StringLength(64)]
        public string Position { get; set; }

        [StringLength(32)]
        public string WorkNature { get; set; } //Full Time {0}, PartTime {1}

        [StringLength(32)]
        public string Supervisor { get; set; }

        public decimal HourlyPayAtStart { get; set; }

        public decimal HourlyPayAtPresent { get; set; }

        [StringLength(256)]
        public string ReasonLeaving { get; set; }

        
        
    }
}


 
