﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Web.Models
{
    public class StoreVM
    {
        
        public Guid? Id { get; set; }

        [StringLength(32), MaxLength(32)]
        public string Code { get; set; }

        public bool IsActive { get; set; }

        public Guid? SupervisorId { get; set; }
        public Employee Supervisor { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }

        [Display(Name = "Store Name"), Required(), StringLength(32), MaxLength(32)]
        public string Name { get; set; }
        

        [Display(Name = "Email Address"), StringLength(64), MaxLength(64), Required(), RegularExpression(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$", ErrorMessage = "Not a valid email address!")]
        [Remote("IsEmailExists", "Store", ErrorMessage = "Email Address is not available", AdditionalFields = "InitialEmail")]
        public string Email { get; set; }

        [Required(), StringLength(16), MaxLength(16), RegularExpression("^[0-9()-]+$", ErrorMessage = "Phone Number should be numberic!")]
        public string Phone { get; set; }

        [Display(Name = "Street Address"), Required(), StringLength(256), MaxLength(256)]
        public string Address { get; set; }

        [Display(Name = "City"), Required()]
        public Guid? CityId { get; set; }
        public City City { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }

        [Display(Name = "State"), Required()]
        public Guid? StateId { get; set; }
        public State State { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }

        [Display(Name = "Zip Code"), Required(ErrorMessage = "Please specify Zip Code!"), StringLength(16), MaxLength(16)]
        public string ZipCode { get; set; }
                

    }


    public static class StoreVMExtensions
    {

        public static StoreVM ToStoreVM(this Store model)
        {
            var viewModel = new StoreVM();
            Utils.Map(model, viewModel);
            return viewModel;
        }

        public static List<StoreVM> ToStoreVM(this IEnumerable<Store> model)
        {
            var viewModel = new List<StoreVM>();
            Utils.Map(model, viewModel);
            return viewModel;
        }


        public static Store ToModel(this StoreVM viewModel, Store store = null)
        {
            if (store == null) store = new Store();

            Utils.Map(viewModel, store);
            return store;
        }


        //public static StoreVM ToStoreVM(this Store model)
        //{
        //    var viewModel = new StoreVM
        //    {
        //        Id = model.Id,
        //        Code = model.Code,
        //        Name = model.Name,
        //        SupervisorId = model.SupervisorId,
        //        Email = model.Email,
        //        Phone = model.Phone,
        //        Address = model.Address,
        //        CityId = model.CityId,
        //        City = model.City,
        //        StateId = model.StateId,
        //        State = model.State,
        //        ZipCode = model.ZipCode,
        //        Supervisor = model.Supervisor
        //    };

        //    return viewModel;
        //}

        //public static Store ToModel(this StoreVM viewModel, Store store = null)
        //{
        //    if (store == null) store = new Store();

        //    store.Id = viewModel.Id ?? Guid.Empty;
        //    store.Code = viewModel.Code;
        //    store.Name = viewModel.Name;
        //    store.SupervisorId = viewModel.SupervisorId;
        //    store.Supervisor = viewModel.Supervisor;
        //    store.Email = viewModel.Email;
        //    store.Phone = viewModel.Phone;            
        //    store.Address = viewModel.Address;
        //    store.CityId = viewModel.CityId;
        //    store.State = viewModel.State;
        //    store.StateId = viewModel.StateId;
        //    store.ZipCode = viewModel.ZipCode;

        //    return store;
        //}

    }


  


}