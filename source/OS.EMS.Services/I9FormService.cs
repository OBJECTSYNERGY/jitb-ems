﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    public interface II9FormService : IBusinessService
    {
        bool Create(I9FormUS model);
        bool Update(I9FormUS model);
        I9FormUS Get(Guid id);
        I9FormUS GetByEmployeeId(Guid id);
        IEnumerable<Employee> GetAll();
    }

    public class I9FormService : II9FormService    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        public I9FormService(IAppUow uow, ILogger logger)
        {
            Uow = uow;
            Logger = logger;
        }
        
        public bool Create(I9FormUS model)
        {
            try
            {
                //Make it Active
                model.IsActive = true;

                Uow.I9FormsUS.Add(model);
                Uow.Commit();

                var employee =  Uow.Employees.GetById(model.EmployeeId.Value);
                employee.I9FormId = model.Id;
                Uow.Employees.Update(employee);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create Employee");
                return false;
            }
        }

        public bool Update(I9FormUS model)
        {
            try
            {
                Uow.I9FormsUS.Update(model);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update I9FormUS");
                return false;
            }
        }

        public I9FormUS Get(Guid id)
        {
            return Uow.I9FormsUS.GetById(id);
        }

        public I9FormUS GetByEmployeeId(Guid id)
        {
            //Search at i9 Table if not available then find emply with same id, and extract information from their ti new i9Form model
            var model = Uow.I9FormsUS.GetAll().SingleOrDefault(e => e.EmployeeId == id);
            if (model != null) return model;

            var employee = Uow.Employees.GetById(id);
            model = new I9FormUS();
            model.Id = Guid.Empty;
            model.EmployeeId = employee.Id;
            FillI9FormUS(model, employee);

            return model;
        }

        public IEnumerable<Employee> GetAll()
        {
            return Uow.Employees.GetAll();
        }

        private void FillI9FormUS(I9FormUS i9, Employee employee)
        {
            i9.LastName = employee.LastName;
            i9.FirstName = employee.FirstName;
            i9.DateOfBirth = employee.DateOfBirth;

            i9.MiddleInitial = employee.MiddleInitial;
            i9.StreetAddress = employee.Address;
            i9.City = employee.City != null ? employee.City.Name : string.Empty;
            i9.State = employee.State != null ? employee.State.Name : string.Empty;
            i9.ZipCode = employee.ZipCode;
            i9.Phone = employee.Phone;
            i9.Email = employee.Email;
            
            

        }

    } 
}
