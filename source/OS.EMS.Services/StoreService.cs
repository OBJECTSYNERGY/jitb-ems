﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Services
{
    public interface IStoreService : IBusinessService
    {
        bool Create(Store store);
        bool Update(Store store);
        IEnumerable<Store> GetAll();
        bool Activate(Guid id, bool active);
        Store Get(Guid id);
        Store Get(string code);
        IEnumerable<Store> GetByManagerId(Guid managerId);
        Store GetLast();

        IEnumerable<Store> SearchByCode(string storeCode);
        IEnumerable<Store> SearchByName(string storeName);

        int TotalStoresCount(bool isActive);

        bool SetSupervisor(Guid id, Guid eid, Guid? peid);

        bool IsEmailExists(string emailAddress);
    }

    public class StoreService : IStoreService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        //For other Services
        public StoreService(IAppUow uow)
        {
            Uow = uow;
        }

        #region CRUD Operations
        public bool Create(Store store)
        {
            try
            {
                //For Store Code Durign Create
                var lastStore = GetLast();
                if (lastStore != null)
                {
                    int storeSufix = int.Parse(lastStore.Code.Substring(AppConstants.StorePrefix.Length));
                    store.Code = string.Format("{0}{1}", AppConstants.StorePrefix, ++storeSufix);
                }
                else
                {
                    int applicationSeed = int.Parse(AppConstants.ApplicationSeed);
                    store.Code = string.Format("{0}{1}", AppConstants.StorePrefix, ++applicationSeed);
                }

                store.IsActive = true;

                //For Create Command
                Uow.Stores.Add(store);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create Store");
                return false;
            }
        }

        public bool Update(Store store)
        {
            try
            {
                Uow.Stores.Update(store);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update Store");
                return false;
            }
        }

        public IEnumerable<Store> GetAll()
        {
            return Uow.Stores.GetAll().OrderBy(s => s.Name);
        }

        public bool Activate(Guid id, bool active)
        {
            try
            {
                var store = Get(id);
                store.IsActive = active;
                Uow.Stores.Update(store);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Activate Application Store");
                return false;
            }
        }

        public Store Get(Guid Id)
        {
            return Uow.Stores.GetById(Id);
        }

        public Store Get(string code)
        {
            return Uow.Stores.GetAll()
                    .SingleOrDefault(s => s.Code == code);
        }

        public IEnumerable<Store> GetByManagerId(Guid managerId)
        {
            return Uow.Stores.GetAll().Where(x => x.SupervisorId == managerId);
        }

        public Store GetLast()
        {
            var stores = Uow.Stores.GetAll().OrderByDescending(x => x.CreatedOn);
            if (stores.Any())
            {
                return stores.FirstOrDefault();
            }
            return null;
        }

        public IEnumerable<Store> SearchByCode(string storeCode)
        {
            return Uow.Stores.GetAll().Where(x => x.Code.ToLower().Contains(storeCode.ToLower()));
        }

        public IEnumerable<Store> SearchByName(string storeName)
        {
            return Uow.Stores.GetAll().Where(x => x.Name.ToLower().Contains(storeName));
        }

        public int TotalStoresCount(bool isActive)
        {
            return Uow.Stores.GetAll().Count(x => x.IsActive == isActive);
        }

        public bool SetSupervisor(Guid id, Guid eid, Guid? peid)
        {
            var store = Get(id);
            store.SupervisorId = eid;
            bool statusSuccess = false;
            if (Update(store))
            {
                //For New Store Manager
                var employee = Uow.Employees.GetById(eid);
                employee.IsManager = true;
                Uow.Employees.Update(employee);

                statusSuccess = true;
                //For Previous Store Manager
                //TODO: check if previous manager manages, more store do not mark false, currently only one manager per store/Office
                if (peid.HasValue)
                {
                    var prevEmployee = Uow.Employees.GetById(peid.Value);
                    prevEmployee.IsManager = false;
                    Uow.Employees.Update(prevEmployee);

                }
            }
            return statusSuccess;
        }

        public bool IsEmailExists(string emailAddress)
        {
            var models = Uow.Stores.GetAll().Where(s => s.Email == emailAddress);
            return models.Any() ? true : false;
        }

        #endregion
    }
}
