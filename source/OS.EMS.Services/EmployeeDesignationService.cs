﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    public interface IEmployeeDesignationService : IBusinessService
    {
        bool Create(EmployeeDesignation employeeDesignation);
        bool Update(EmployeeDesignation employeeDesignation);
        IEnumerable<EmployeeDesignation> GetAll();
        EmployeeDesignation Get(Guid id);
        bool Activate(Guid id, bool active);
    }

    public class EmployeeDesignationService : IEmployeeDesignationService
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        //For other Services
        //private IEmployeeService EmployeeService { get; set; }

        public EmployeeDesignationService(IAppUow uow)
        {
            Uow = uow;
        }

       
        public bool Create(EmployeeDesignation employeeDesignation)
        {
            try
            {
                //Make it Active
                employeeDesignation.IsActive = true;

                Uow.EmployeeDesignation.Add(employeeDesignation);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create EmployeeDesignation");
                return false;
            }            
        }

        public bool Update(EmployeeDesignation employeeDesignation)
        {
            try
            {
                Uow.EmployeeDesignation.Update(employeeDesignation);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update employeeDesignation");
                return false;
            }     
        }

        public IEnumerable<EmployeeDesignation> GetAll()
        {
            return Uow.EmployeeDesignation.GetAll();
        }

        public EmployeeDesignation Get(Guid id)
        {
            return Uow.EmployeeDesignation.GetById(id);
        }

        public bool Activate(Guid id, bool active)
        {
            try
            {
                var employeeDesignation = Get(id);
                employeeDesignation.IsActive = active;
                Uow.EmployeeDesignation.Update(employeeDesignation);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Activate Application Store");
                return false;
            }
        }

    } 
}
