﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OS.EMS.Model
{
    public class EmployeeDesignation : Entity
    {
        [StringLength(64)]
        public string Designation { get; set; }
    }
}


 
