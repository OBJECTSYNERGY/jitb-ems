﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    public interface IAcceptableDocumentServices : IBusinessService
    {
        bool Create(AcceptableDocument acceptableDocument);
        bool Update(AcceptableDocument acceptableDocument);
        IEnumerable<AcceptableDocument> GetAll();
        AcceptableDocument Get(Guid id);
        IEnumerable<AcceptableDocument> GetByI9FormId(Guid id);
    }

    public class AcceptableDocumentServices : IAcceptableDocumentServices
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        public AcceptableDocumentServices(IAppUow uow)
        {
            Uow = uow;
        }


        public bool Create(AcceptableDocument acceptableDocument)
        {
            try
            {
                Uow.AcceptableDocuments.Add(acceptableDocument);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create AcceptableDocument");
                return false;
            }            
        }

        public bool Update(AcceptableDocument acceptableDocument)
        {
            try
            {
                Uow.AcceptableDocuments.Update(acceptableDocument);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update AcceptableDocument");
                return false;
            }     
        }

        public IEnumerable<AcceptableDocument> GetAll()
        {
            return Uow.AcceptableDocuments.GetAll();
        }

        public AcceptableDocument Get(Guid Id)
        {
            return Uow.AcceptableDocuments.GetById(Id);
        }

        public IEnumerable<AcceptableDocument> GetByI9FormId(Guid id)
        {
            return Uow.AcceptableDocuments.GetAll().Where(x => x.I9FormId == id);
        }
     
    } 
}
