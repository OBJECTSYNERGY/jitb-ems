﻿using System.Data.Entity.ModelConfiguration;
using OS.EMS.Model;

namespace OS.EMS.Data.Configurations
{
    public class StoreConfiguration : EntityTypeConfiguration<Store>
    {
        public StoreConfiguration()
        {
            //Explicitly defined this relation, as there is a circular dependency of each other
            //HasRequired(s => s.Supervisor).WithMany().HasForeignKey(s => s.SupervisorId);            
            HasOptional(x => x.Supervisor).WithMany().HasForeignKey(s => s.SupervisorId);
        }
    }
}
