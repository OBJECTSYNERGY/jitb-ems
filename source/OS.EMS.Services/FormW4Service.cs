﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using OS.EMS.Data;
using OS.EMS.Model;

namespace OS.EMS.Services
{
    
    public interface IFormW4Service : IBusinessService
    {

        bool Create(FormW4 model);
        bool Update(FormW4 model);
        FormW4 Get(Guid id);
        FormW4 GetByEmployeeId(Guid id);
        IEnumerable<Employee> GetAll();
    }

    public class FormW4Service : IFormW4Service
    {
        private IAppUow Uow { get; set; }
        private ILogger Logger { get; set; }

        public FormW4Service(IAppUow uow, ILogger logger)
        {
            Uow = uow;
            Logger = logger;
        }

        public bool Create(FormW4 model)
        {
            try
            {
                //Make it Active
                model.IsActive = true;

                Uow.FormW4.Add(model);
                Uow.Commit();

                //For Employee FormW4 Status
                var employee = Uow.Employees.GetById(model.EmployeeId.Value);
                employee.FormW4Id = model.Id;
                Uow.Employees.Update(employee);
                Uow.Commit();

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Create Employee");
                return false;
            }
        }

        public bool Update(FormW4 model)
        {
            try
            {
                Uow.FormW4.Update(model);
                Uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Error executing: {0}", "Update FormW4");
                return false;
            }
        }

        public FormW4 Get(Guid id)
        {
            return Uow.FormW4.GetById(id);
        }

        public FormW4 GetByEmployeeId(Guid id)
        {
            var model = Uow.FormW4.GetAll().SingleOrDefault(e => e.EmployeeId == id);
            if (model != null) return model;

            var employee = Uow.Employees.GetById(id);
            model = new FormW4();
            model.Id = Guid.Empty;
            model.EmployeeId = employee.Id;

            return model;

        }

        public IEnumerable<Employee> GetAll()
        {
            return Uow.Employees.GetAll();
        }

      
    } 
}
