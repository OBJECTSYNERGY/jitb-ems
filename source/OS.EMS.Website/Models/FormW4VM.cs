﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OS.EMS.Model;
using OS.EMS.Model.Helper;

namespace OS.EMS.Web.Models
{
    public class FormW4VM
    {
        public Guid? Id { get; set; }
        public Guid EmployeeId { get; set; }
        public bool IsActive { get; set; }
        
        //For Personal Allownance Worksheet
        public string Allowance_A { get; set; }
        public string Allowance_B { get; set; }
        public string Allowance_C { get; set; }
        public string Allowance_D { get; set; }
        public string Allowance_E { get; set; }
        public string Allowance_F { get; set; }
        public string Allowance_G { get; set; }
        public string Allowance_H { get; set; }

        //For Employee's Withholding Allowance Certificate
        public string MartialStatusType { get; set; }


        public int TotalAllowancesClaming_5 { get; set; }
        public int AdditionalAmtWithheld_6 { get; set; }
        public string Allowance_7 { get; set; }

        //Deductions and Adjustments
        public int DedAndAdj_1 { get; set; }
        public int DedAndAdj_2 { get; set; }
        public int DedAndAdj_3 { get; set; }
        public int DedAndAdj_4 { get; set; }
        public int DedAndAdj_5 { get; set; }
        public int DedAndAdj_6 { get; set; }
        public int DedAndAdj_7 { get; set; }
        public int DedAndAdj_8 { get; set; }
        public int DedAndAdj_9 { get; set; }
        public int DedAndAdj_10 { get; set; }

        //Deductions and Adjustments -  (Two-Earners/Multiple Jobs Worksheet)
        public int DedAndAdj_TwoEarners_1 { get; set; }
        public int DedAndAdj_TwoEarners_2 { get; set; }
        public int DedAndAdj_TwoEarners_3 { get; set; }
        public int DedAndAdj_TwoEarners_4 { get; set; }
        public int DedAndAdj_TwoEarners_5 { get; set; }
        public int DedAndAdj_TwoEarners_6 { get; set; }
        public int DedAndAdj_TwoEarners_7 { get; set; }
        public int DedAndAdj_TwoEarners_8 { get; set; }
        public int DedAndAdj_TwoEarners_9 { get; set; }

    }


    public static class FormW4VMExtensions
    {
        public static FormW4VM ToViewModel(this FormW4 model)
        {
            var viewModel = new FormW4VM();

            //For FormW4
            Utils.Map(model, viewModel);

            return viewModel;
        }

        public static FormW4 ToModel(this FormW4VM viewModel, FormW4 model = null)
        {
            if (model == null) model = new FormW4();

            //For FormW4
            Utils.Map(viewModel, model);

            return model;
        }

    }

}