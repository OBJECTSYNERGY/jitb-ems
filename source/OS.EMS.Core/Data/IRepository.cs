﻿using System;
using System.Linq;
using OS.EMS.Model;

namespace OS.EMS.Data
{
    public interface IRepository<T> 
    {
        IQueryable<T> GetAll();
        //T GetById(int id);
        T GetById(Guid id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Guid id);
    }
}
