﻿using System;
using System.IO;
using System.Xml.Linq;

namespace OS.EMS
{
    public interface ISettings
    {
        string Name { get; }

        void Load(XElement element);
        XElement ToXElement();
        void SetDefaults();
    }

    public abstract class Settings : ISettings
    {
        protected Settings()
        {
            LoadSettings();
        }

        private void LoadSettings()
        {
            var reader = new System.Configuration.AppSettingsReader();
            
            var path = AppDomain.CurrentDomain.BaseDirectory;            
            var settingFolder = reader.GetValue("SettingFolder", Type.GetType("System.String")).ToString();
            var fileName = string.Format("{0}.xml", Name);

            path = Path.Combine(path, settingFolder, fileName);

            XElement element;
            try
            {
                SetDefaults();
                element = XElement.Load(path);
                Load(element);
                return;
            }
            catch (Exception e)
            {
                //Logger.Log(e);
            }

            SetDefaults();

            element = ToXElement();
            element.Save(path);
        }

        public abstract string Name { get; }
        
        public abstract void Load(XElement element);
        public abstract XElement ToXElement();

        public abstract void SetDefaults();
    }
}
