﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Model.Helper;
using OS.EMS.Services;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{
    public class EmployeeController : BaseController
    {

        [Inject]
        public EmployeeController(IEmployeeService employeeService, IApplicantService applicantService, IStoreService storeService, IAttendanceService attendanceService, IAttendanceDataService attendanceDataService, IGeographyService geographyService, ISecurityService securityService)
        {
            ApplicantService = applicantService;
            EmployeeService = employeeService;
            AttendanceService = attendanceService;
            AttendanceDataService = attendanceDataService;
            StoreService = storeService;
            GeographyService = geographyService;
            SecurityService = securityService;
        }

        #region CRUD Operations

        public ActionResult Create()
        {
            var viewModel = new EmployeeVM();

            if (TempData["NewEmployee"] != null)
            {
                viewModel = TempData["NewEmployee"] as EmployeeVM;
            }

            //For City & State SLI
            PopulateSLI(viewModel);

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Create(EmployeeVM viewModel)
        {
            if (viewModel == null || !viewModel.ApplicantId.HasValue) return RedirectToAction("InvalidRequest", "Error");

            var employee = viewModel.ToModel();

            if (EmployeeService.Create(employee))
            {
                ShowMessage(Messages.CreateEmployeeSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult Update(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var employee = EmployeeService.Get(id.Value);
            var viewModel = employee.ToEmployeeVM();
            PopulateSLI(viewModel);

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Update(EmployeeVM viewModel)
        {
            if (viewModel == null || viewModel.Id == null) return RedirectToAction("InvalidRequest", "Error");

            var employee = viewModel.ToModel();
            if (EmployeeService.Update(employee))
            {
                ShowMessage(Messages.UpdateEmployeeSuccess, MessageType.Success);
                return RedirectToAction("Details", new { id = employee.Id });
            }

            return RedirectToAction("InvalidRequest", "Error");
        }

        public ActionResult List()
        {
            var employees = EmployeeService.GetAll();
            var employeesVM = employees.ToEmployeeVM();

            return View(employeesVM);
        }

        public ActionResult ManagersList()
        {
            var employees = EmployeeService.GetManagers(true);
            var employeesVM = employees.ToEmployeeVM();

            return View("ManagersList", employeesVM);
        }

        public ActionResult Activate(Guid? id, bool? active)
        {
            if (!id.HasValue || !active.HasValue) return RedirectToAction("InvalidRequest", "Error");

            if (EmployeeService.Activate(id.Value, active.Value))
            {
                ShowMessage(active.Value ? Messages.ActivateEmployeeSuccess : Messages.DeactivateEmployeeSuccess, MessageType.Success);
                return RedirectToAction("ManagersList");
            }

            return RedirectToAction("ManagersList");
        }

        [HttpPost]
        public ActionResult SearchByCode(FormCollection frmColl, string employeeCode)
        {
            if (frmColl == null || string.IsNullOrEmpty(employeeCode))
            {
                return RedirectToAction("Dashboard", "Home");
            }

            var user = HttpContext.Session["AppUser"] as SystemUser;
            if (user == null) return RedirectToAction("InvalidRequest", "Error");

            var role = SecurityService.GetRolesById(user.RoleId.Value);

            IEnumerable<Employee> employees = null;
            if (user.EmployeeId.HasValue && user.Role.Name == RoleType.Manager.ToString())
            {
                var stores = StoreService.GetByManagerId(user.EmployeeId.Value);
                if (stores.Any())
                {
                    employees = EmployeeService.SearchByCode(employeeCode, stores.FirstOrDefault().Id);
                }
            }
            else if (user.Role.Name == RoleType.Admin.ToString())
            {
                employees = EmployeeService.SearchByCode(employeeCode);
            }


            return SearchByExt(employees);
        }

        [HttpPost]
        public ActionResult SearchByName(FormCollection frmColl, string employeeName)
        {
            if (frmColl == null || string.IsNullOrEmpty(employeeName))
            {
                return RedirectToAction("Dashboard", "Home");
            }

            var user = HttpContext.Session["AppUser"] as SystemUser;
            if (user == null) return RedirectToAction("InvalidRequest", "Error");

            var role = SecurityService.GetRolesById(user.RoleId.Value);

            IEnumerable<Employee> employees = null;
            if (user.EmployeeId.HasValue && user.Role.Name == RoleType.Manager.ToString())
            {
                var stores = StoreService.GetByManagerId(user.EmployeeId.Value);
                if (stores.Any())
                {
                    employees = EmployeeService.SearchByName(employeeName, stores.FirstOrDefault().Id);
                }
            }
            else if (user.Role.Name == RoleType.Admin.ToString())
            {
                employees = EmployeeService.SearchByName(employeeName);
            }

            return SearchByExt(employees);

        }

        public ActionResult Details(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var employee = EmployeeService.Get(id.Value);
            var viewModel = employee.ToEmployeeVM();

            ForAttendanceCal(viewModel);

            var dtFrom = DateTime.Now.Date;

            AttendanceHelper(viewModel, dtFrom.AddDays(-2), dtFrom.AddHours(24), true, DateTime.Now);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AssignStore(FormCollection frmColl)
        {

            if (frmColl == null || string.IsNullOrWhiteSpace(frmColl["id"]) || string.IsNullOrWhiteSpace(frmColl["StoreId"])) return RedirectToAction("NotFound", "Error");

            Guid employeeID;
            var statusCreate = EmployeeService.AssignStore(Guid.Parse(frmColl["id"]), Guid.Parse(frmColl["StoreId"]), out employeeID);

            if (statusCreate)
            {
                ShowMessage(Messages.AssignStoreSuccess, MessageType.Success);
                return RedirectToAction("Details", "Employee", new { id = employeeID });
            }

            return RedirectToAction("List");

        }

        public ActionResult SearchAttendance(FormCollection frmColl)
        {
            if (frmColl == null && string.IsNullOrEmpty(frmColl["hfEmployeeID"])) return RedirectToAction("NotFound", "Error");


            var employee = EmployeeService.Get(Guid.Parse(frmColl["hfEmployeeID"]));
            var viewModel = employee.ToEmployeeVM();

            //For AttendanceInfo
            //AttendanceInfoVM attendanceInfo = new AttendanceInfoVM();
            //EmployeeVM viewModel = new EmployeeVM();
            //viewModel.AttendanceInfo.EmployeeId = viewModel.Id;

            //For Working Hours
            ForAttendanceCal(viewModel);

            var currentDate = DateTime.Now;
            var dtFrom = DateTime.Now.AddDays(-1);
            var dtTo = currentDate;
            var IsDefaultAttendace = true;
            if (!string.IsNullOrWhiteSpace(frmColl["FromDate"]) && (!string.IsNullOrWhiteSpace(frmColl["ToDate"])))
            {
                dtFrom = DateTime.Parse(frmColl["FromDate"]);
                dtTo = DateTime.Parse(frmColl["ToDate"]);
                IsDefaultAttendace = false;
            }

            viewModel.AttendanceInfo.FromDate = dtFrom;
            viewModel.AttendanceInfo.ToDate = dtTo;

            AttendanceHelper(viewModel, dtFrom, dtTo, IsDefaultAttendace, currentDate);

            return PartialView("_Attendance", viewModel.AttendanceInfo);
        }

        #endregion

        #region AppHelper

        private void PopulateSLI(EmployeeVM viewModel)
        {
            var states = GeographyService.GetStates();
            viewModel.States = states.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = s.Name });

            var cities = GeographyService.GetCities();
            viewModel.Cities = cities.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });

            var stores = StoreService.GetAll();
            viewModel.Stores = stores.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name });

        }

        private ActionResult SearchByExt(IEnumerable<Employee> employees)
        {
            var employeesVM = employees.ToEmployeeVM();
            if (employeesVM.Count() == 1)
            {
                var viewModel = employeesVM.FirstOrDefault();
                //PopulateSLI(viewModel);

                //For Employees Attendance
                //TODO:Attendance
                ForAttendanceCal(viewModel);
                var dtFrom = DateTime.Now.Date;

                AttendanceHelper(viewModel, dtFrom, dtFrom.AddHours(24), true, DateTime.Now);

                //var employeeAttendances = AttendanceDataService.GetByDate(viewModel.Code, dtFrom, dtFrom.AddHours(24));
                //viewModel.AttendanceInfo.AttendancesData = employeeAttendances;

                return View("Details", viewModel);
            }
            else if (employeesVM.Count() > 1)
            {
                return View("List", employeesVM);
            }

            ShowMessage(Messages.SearchSearchResults, MessageType.Error);
            return RedirectToAction("Dashboard", "Home");
        }

        [HttpGet]
        public virtual JsonResult IsEmailExists(string Email, string InitialEmail)
        {
            //Thread.Sleep(2000);//fake delay
            if (Email == InitialEmail)
            {
                return this.Json(true, JsonRequestBehavior.AllowGet);
            }
            return this.Json(!EmployeeService.IsEmailExists(Email), JsonRequestBehavior.AllowGet);
        }

        private void AttendanceHelper(EmployeeVM viewModel, DateTime dtFrom, DateTime dtTo, bool IsDefaultAttendace, DateTime currentDate)
        {
            var employeeAttendances = AttendanceDataService.GetByDate(viewModel.AttendanceCode, dtFrom, dtTo.AddHours(24));

            //For Attendance Categorization
            if (IsDefaultAttendace)
            {
                //For Today Attendance
                var dateWorkStart = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day).AddHours(7);
                //dateWorkStart.AddHours(-currentDate.Hour).AddMinutes(-currentDate.Minute).AddHours(7);
                viewModel.AttendanceInfo.AttendancesToday = employeeAttendances.Where(a => a.Time > dateWorkStart);

                //For Yesterday Attendance
                viewModel.AttendanceInfo.AttendancesYesterday =
                    employeeAttendances.Where(a => a.Time > dateWorkStart.AddDays(-1) && a.Time < dateWorkStart);

                //For Last Week Attendance
                //DateTime currentWeekDate = currentDate;
                //currentWeekDate = new DateTime(currentWeekDate.Year, currentWeekDate.Month, currentWeekDate.Day);
                //DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(currentWeekDate);
                //switch (day)
                //{
                //    case DayOfWeek.Monday:
                //        currentWeekDate = currentWeekDate.AddDays(0);
                //        break;
                //    case DayOfWeek.Tuesday:
                //        currentWeekDate = currentWeekDate.AddDays(-1);
                //        break;
                //    case DayOfWeek.Wednesday:
                //        currentWeekDate = currentWeekDate.AddDays(-2);
                //        break;
                //    case DayOfWeek.Thursday:
                //        currentWeekDate = currentWeekDate.AddDays(-3);
                //        break;
                //    case DayOfWeek.Friday:
                //        currentWeekDate = currentWeekDate.AddDays(-4);
                //        break;
                //    case DayOfWeek.Saturday:
                //        currentWeekDate = currentWeekDate.AddDays(-5);
                //        break;
                //    case DayOfWeek.Sunday:
                //        currentWeekDate = currentWeekDate.AddDays(-6);
                //        break;
                //}

                //var currentWeekAttendance = employeeAttendances.Where(a => a.Time > currentWeekDate);
            }
            else
            {
                viewModel.AttendanceInfo.AttendancesSearched = employeeAttendances;
            }

            viewModel.AttendanceInfo.IsDefaultAttendace = IsDefaultAttendace;
        }

        //public ActionResult AssignStore(Guid? id)
        //{
        //    if (id == null) return RedirectToAction("NotFound", "Error");

        //    var applicant = ApplicantService.Get(id.Value);
        //    var viewModel = applicant.ToApplicantVM();
        //    PopulateGeographySLI(viewModel);

        //    //For New Employee from Applicant Genral Info
        //    var employeeVM = new EmployeeVM();
        //    Utils.Map(applicant, employeeVM);

        //    //Assign & Reset id's values
        //    employeeVM.Id = null;
        //    employeeVM.Code = null;
        //    employeeVM.ApplicantId = applicant.Id;

        //    TempData["NewEmployee"] = employeeVM;

        //    return RedirectToAction("Create", "Employee");

        //}

        //[HttpPost]
        //public ActionResult AssignStore(FormCollection frmColl)
        //{

        //    if (frmColl == null) return RedirectToAction("NotFound", "Error");

        //    var applicant = ApplicantService.Get(Guid.Parse(frmColl["id"]));
        //    var viewModel = applicant.ToApplicantVM();
        //    //PopulateGeographySLI(viewModel);

        //    //For New Employee from Applicant Genral Info
        //    var employeeVM = new EmployeeVM();
        //    Utils.Map(applicant, employeeVM);

        //    //Assign & Reset id's values
        //    employeeVM.Id = null;
        //    employeeVM.Code = null;
        //    employeeVM.ApplicantId = applicant.Id;
        //    employeeVM.StoreId = Guid.Parse(frmColl["StoreId"]);

        //    //To Model for Saving
        //    var employee = employeeVM.ToModel();

        //    if (EmployeeService.Create(employee))
        //    {
        //        applicant.IsActive = true;
        //        if (ApplicantService.Update(applicant))
        //        {
        //            ShowMessage(Messages.AssignStoreSuccess, MessageType.Success);
        //            //return RedirectToAction("List");
        //            return RedirectToAction("Details", "Employee", new{ id=employee.Id});
        //        }
        //    }

        //    //TempData["NewEmployee"] = employeeVM;
        //    //return RedirectToAction("Create", "Employee");
        //    return RedirectToAction("List");

        //}

        private void ForAttendanceCal(EmployeeVM viewModel)
        {
            //For Working Hours
            double workingHours = 40;
            double tax = 0.025;
            double totalWorkingHours = (viewModel.SaleryRate * workingHours);

            viewModel.AttendanceInfo.SaleryRate = viewModel.SaleryRate;
            viewModel.AttendanceInfo.WorkingHours = totalWorkingHours;
            viewModel.AttendanceInfo.SaleryPayable = totalWorkingHours - (totalWorkingHours * tax);

            //For Employees Attendance
            viewModel.AttendanceInfo.EmployeeId = viewModel.Id;

        }

        //private void EmployeeAttendances(Guid? id)
        //{
        //    if (id.HasValue)
        //    {
        //        var employeeAttendances = AttendanceService.GetByEmployeeId(id.Value);
        //        var employeeAttendancesVM = new List<AttendanceVM>();
        //        Utils.Map(employeeAttendances, employeeAttendancesVM);
        //        ViewData["EmployeeAttendances"] = employeeAttendancesVM;
        //    }
        //}

        //private void EmployeeAttendances(string code, DateTime fromDate, DateTime toDate)
        //{
        //    var employeeAttendances  = AttendanceDataService.GetByDate(code, fromDate, toDate);
        //    ViewData["EmployeeAttendances"] = employeeAttendances;
        //    //AttendancesData

        //}

        #endregion

    }


}