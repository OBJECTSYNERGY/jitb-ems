using System;
using System.ComponentModel.DataAnnotations;

namespace OS.EMS.Web.Models
{
    public class SignInVM
    {
        
        [Display(Name = "Email Address"), Required(), StringLength(64), MaxLength(64), RegularExpression(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")]
        public string EmailAddress { get; set; }

        [Required(), DataType(DataType.Password), StringLength(32), MaxLength(32)]
        public string Password { get; set; }
    }
}