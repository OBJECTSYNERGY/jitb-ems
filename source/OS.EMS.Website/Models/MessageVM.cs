﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OS.EMS.Web.Models
{
    public class MessageVM
    {
        public string Text { get; set; }
        public string Type { get; set; }
    }
}