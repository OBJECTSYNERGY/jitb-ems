﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Services;

namespace OS.EMS.Web.Models
{
    public class ApplicantEducationVM
    {
        public ApplicantEducationVM() {
            
        }

        [Inject]
        public ApplicantEducationVM(IApplicantService applicantService)
        {
            //var models = applicantService.GetEducationType();
            //EducationTypes = models.Select(e => new SelectListItem { Value = e.Id.ToString(), Text = e.Title });
        }

        public Guid Id { get; set; }
        public Guid ApplicantId { get; set; }
        //public virtual Applicant Applicant { get; set; }

        public Guid EducationTypeId { get; set; }
        public IEnumerable<SelectListItem> EducationTypes { get; set; }

        [StringLength(32), MaxLength(32)]
        public string Name { get; set; }

        [StringLength(128), MaxLength(128)]
        public string Location { get; set; }
        
        public int Years { get; set; }
        
        public string IsGraduated { get; set; }

        public bool GED { get; set; }
        public bool IsEnrolled { get; set; }

        public int IntCounter { get; set; }

    }
}