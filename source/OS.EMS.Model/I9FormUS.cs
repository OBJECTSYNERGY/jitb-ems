﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OS.EMS.Model
{
    public class I9FormUS : AuditEntity
    {
        public I9FormUS()
        {
            DocumentsA = new Collection<AcceptableDocument>();
            //DocumentsB = new Collection<AcceptableDocument>();
            //DocumentsC = new Collection<AcceptableDocument>();
        }

        
        public Guid? EmployeeId { get; set; }
        //public virtual Employee Employee { get; set; }


        [StringLength(32)]
        public string LastName { get; set; }

        [StringLength(32)]
        public string FirstName { get; set; }

        [StringLength(16)]
        public string MiddleInitial { get; set; }

        [StringLength(32)]
        public string OtherNames { get; set; }

        [StringLength(128)]
        public string StreetAddress { get; set; }

        [StringLength(8)]
        public string ApartmentNumber { get; set; }

        [StringLength(32)]
        public string City { get; set; }

        [StringLength(32)]
        public string State { get; set; }

        [StringLength(16)]
        public string ZipCode { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [StringLength(32)]
        public string SocialSecurityNumber { get; set; }

        [StringLength(64)]
        public string Email { get; set; }

        [StringLength(16)]
        public string Phone { get; set; }

        public bool IsCitizen { get; set; }
        public bool IsNonCitizenNational { get; set; }
        public bool IsLawfulResident { get; set; }
        public bool IsAlienAuthorized { get; set; }
        public DateTime? AlienAuthorizedExpiryDate { get; set; }

        [StringLength(32)]
        public string AlienRegistrationNumber { get; set; }

        [StringLength(32)]
        public string I9FormAdmissionNumber { get; set; }

        [StringLength(32)]
        public string ForeignPassportNumber { get; set; }

        [StringLength(32)]
        public string CountryOfIssuance { get; set; }

        public DateTime? SignatureDate { get; set; }

        //Preparer
        public DateTime? PreparerSignatureDate { get; set; }

        [StringLength(32)]
        public string PreparerLastName { get; set; }

        [StringLength(32)]
        public string PreparerFirstName { get; set; }

        [StringLength(128)]
        public string PreparerStreetAddress { get; set; }

        [StringLength(32)]
        public string PreparerCity { get; set; }

        [StringLength(32)]
        public string PreparerState { get; set; }

        [StringLength(8)]
        public string PreparerZipCode { get; set; }
        
        public virtual ICollection<AcceptableDocument> DocumentsA { get; set; }
        //public ICollection<AcceptableDocument> DocumentsB { get; set; }
        //public ICollection<AcceptableDocument> DocumentsC { get; set; }

        public DateTime? FirstDayDate { get; set; }

        //Certification
        public DateTime? RepresentativeSignatureDate { get; set; }

        [StringLength(32)]
        public string RepresentativeTitle { get; set; }

        [StringLength(32)]
        public string RepresentativeLastName { get; set; }

        [StringLength(32)]
        public string RepresentativeFirstName { get; set; }

        [StringLength(32)]
        public string RepresentativeOrganisation { get; set; }

        [StringLength(128)]
        public string RepresentativeStreetAddress { get; set; }

        [StringLength(32)]
        public string RepresentativeCity { get; set; }

        [StringLength(32)]
        public string RepresentativeState { get; set; }

        [StringLength(8)]
        public string RepresentativeZipCode { get; set; }

        //Reverification or Rehires
        [StringLength(32)]
        public string RehireLastName { get; set; }

        [StringLength(32)]
        public string RehireFirstName { get; set; }

        [StringLength(16)]
        public string RehireMiddleInitial { get; set; }
        public DateTime? RehireDate { get; set; }

        [StringLength(32)]
        public string DocumentTitle { get; set; }

        [StringLength(32)]
        public string DocumentNumber { get; set; }
        public DateTime? DocumentExpiryDate { get; set; }

        public DateTime? EmployerSignatureDate { get; set; }

        [StringLength(32)]
        public string EmployerPrintName { get; set; }
   }

   public class AcceptableDocument : Entity 
   {

       public Guid I9FormId { get; set; }

       //[StringLength(32)]
       public string DocumentCode { get; set; }

       [ForeignKey("I9FormId")]
       public virtual I9FormUS I9FormUS { get; set; }

       [StringLength(64)]
       public string DocumentTypeId { get; set; }

       [StringLength(32)]
       public string Title { get; set; }

       [StringLength(32)]
       public string Authority { get; set; }

       [StringLength(32)]
       public string Number { get; set; }

       public DateTime? ExpiryDate { get; set; }
   }
}
