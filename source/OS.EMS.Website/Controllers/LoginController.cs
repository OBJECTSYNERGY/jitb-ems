using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using OS.EMS.Model;
using OS.EMS.Security;
using OS.EMS.Services;
using OS.EMS.Web.Models;

namespace OS.EMS.Web.Controllers
{
    public class LoginController : BaseController
    {
        

        [Inject]
        public LoginController(ISecurityService securityService, ILoginService loginService)
        {
            SecurityService = securityService;
            LoginService = loginService;
        }

        #region CRUD Operations
        [HttpPost]
        public ActionResult SignIn(SignInVM model)
        {
            if (model == null) return RedirectToAction("InvalidRequest", "Error");

            var user = SecurityService.Authenticate(model.EmailAddress, model.Password);
            
            if(user == null)
            {
                ShowMessage(Messages.InvalidEmailPassword, MessageType.Error);
                return RedirectToAction("Index", "Home");
            }

            LoginService.SignIn(user);
            return RedirectToAction("Dashboard", "Home");
        }

        public ActionResult SignOut()
        {
            LoginService.SignOut();
            return RedirectToAction("Index", "Home");
        }
        #endregion
    }
}