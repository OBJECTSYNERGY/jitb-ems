using System.Web.Mvc;

namespace OS.EMS.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult InvalidRequest()
        {
            return View();
        }
    }
}