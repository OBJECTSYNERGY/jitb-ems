using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using OS.EMS.Model;
using OS.EMS.Security;

namespace OS.EMS.Data
{
    /// <summary>
    /// The EMS "Unit of Work"
    ///     1) decouples the repos from the controllers
    ///     2) decouples the DbContext and EF from the controllers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="Customer"/>.
    /// A repository typically exposes "Get" methods for querying and
    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext in EMS).
    /// </remarks>
    public class AppUow : IAppUow, IDisposable
    {
        protected IRepositoryProvider RepositoryProvider { get; set; }
        private ILoginService LoginService { get; set; }

        public AppUow(IRepositoryProvider repositoryProvider, ILoginService loginService)
        {
            CreateDbContext();
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
            LoginService = loginService;
        }

        // App Repositories
        public IRepository<AppCode> AppCodes {get { return GetStandardRepo<AppCode>(); }}
        public IRepository<Country> Countries { get { return GetStandardRepo<Country>(); } }
        public IRepository<State> States { get { return GetStandardRepo<State>(); } }
        public IRepository<City> Cities { get { return GetStandardRepo<City>(); } }

        public IRepository<Role> Roles { get { return GetStandardRepo<Role>(); } }
        public ISystemUsersRepository SystemUsers { get { return GetRepo<ISystemUsersRepository>(); } }
        
        public IStoresRepository Stores { get { return GetRepo<IStoresRepository>(); } }

        public IRepository<Applicant> Applicants { get { return GetStandardRepo<Applicant>(); } }
        public IRepository<EmployeeDesignation> EmployeeDesignation { get { return GetStandardRepo<EmployeeDesignation>(); } }


        public IRepository<ApplicantEducation> ApplicantEducations { get { return GetStandardRepo<ApplicantEducation>(); } }
        public IRepository<EducationType> EducationTypes { get { return GetStandardRepo<EducationType>(); } }
        public IRepository<ApplicantInterview> ApplicantInterviews { get { return GetStandardRepo<ApplicantInterview>(); } }
        public IRepository<ApplicantWorkHistory> ApplicantWorkHistories { get { return GetStandardRepo<ApplicantWorkHistory>(); } }

        public IEmployeeRepository Employees { get { return GetRepo<IEmployeeRepository>(); } }
        public IRepository<I9FormUS> I9FormsUS { get { return GetStandardRepo<I9FormUS>(); } }
        public IRepository<FormW4> FormW4 { get { return GetStandardRepo<FormW4>(); } }

        public IRepository<Attendance> Attendances { get { return GetStandardRepo<Attendance>(); } }
        public IRepository<AttendanceData> AttendancesData { get { return GetStandardRepo<AttendanceData>(); } }
        public IRepository<AcceptableDocument> AcceptableDocuments {get { return GetStandardRepo<AcceptableDocument>(); }}
       

        protected void CreateDbContext()
        {
            DbContext = new AppDbContext();

            // Do NOT enable proxied entities, else serialization fails
            //DbContext.Configuration.ProxyCreationEnabled = false;
            DbContext.Configuration.ProxyCreationEnabled = true;

            // Load navigation properties explicitly (avoid serialization trouble)
            //DbContext.Configuration.LazyLoadingEnabled = false;
            DbContext.Configuration.LazyLoadingEnabled = true;

            // Because Web API will perform validation, we don't need/want EF to do so
            DbContext.Configuration.ValidateOnSaveEnabled = false;

            //DbContext.Configuration.AutoDetectChangesEnabled = false;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        private IRepository<T> GetStandardRepo<T>() where T : class 
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }
        private T GetRepo<T>() where T : class 
        {
            return RepositoryProvider.GetRepository<T>();
        }

        //private AppDbContext DbContext { get; set; }
        public AppDbContext DbContext { get; set; }

        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        public void Commit()
        {
            DateTime auditDate = DateTime.Now;

            var user = LoginService.User;

            //Todo: Need to filter with IEntity instead IAuditEntity
            //foreach (DbEntityEntry<IAuditEntity> entry in DbContext.ChangeTracker.Entries<IAuditEntity>())
            foreach (DbEntityEntry<IEntity> entry in DbContext.ChangeTracker.Entries<IEntity>())
            {

                if (entry.State == EntityState.Unchanged)
                {
                    if (entry.Entity.Id == Guid.Empty)
                    {
                        //entry.Entity.Id = Guid.NewGuid();
                        entry.State = EntityState.Added;
                    }
                    else {
                        entry.State = EntityState.Modified;
                    }
                }

                if (entry.State == EntityState.Added)
                {
                    entry.Entity.Id = Guid.NewGuid();
                }
                
                

                var auditEntry = entry.Entity as IAuditEntity;
                if (auditEntry != null)
                {
                    if (entry.State == EntityState.Added)
                    {
                        auditEntry.CreatedOn = auditDate;
                        auditEntry.CreatedById = user.Id;
                        //entry.Entity.IsActive = true;
                    }
                    else if (entry.State == EntityState.Modified)
                    {
                        auditEntry.ModifiedOn = auditDate;
                        auditEntry.ModifiedById = user.Id;
                    }
                    else if (entry.State == EntityState.Deleted)
                    {
                        entry.State = EntityState.Modified;
                        auditEntry.DeletedOn = auditDate;
                        auditEntry.DeletedById = user.Id;
                    }
                    
                }

            }

            DbContext.SaveChanges();
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion
    }
}