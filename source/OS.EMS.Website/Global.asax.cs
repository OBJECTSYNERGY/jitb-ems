﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using OS.EMS.Model.Helper;


namespace OS.EMS.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            IocConfig.RegisterIoc(GlobalConfiguration.Configuration);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Application.Add("ApplicationCode", 50000);
            ////Application.Add("EmployeeCode", 50000);
            //Application.Add("StoreCode", 100);

            string dirPathEmpDoc = Server.MapPath(AppConstants.EmpDocPath);

            //For Employees Document
            if (!System.IO.Directory.Exists(dirPathEmpDoc))
            {
                System.IO.Directory.CreateDirectory(dirPathEmpDoc);
            }


            //For AttendanceFiles
            string dirPathAttendanceFiles = Server.MapPath(AppConstants.AttendanceFilesPath);
            if (!System.IO.Directory.Exists(dirPathAttendanceFiles))
            {
                System.IO.Directory.CreateDirectory(dirPathAttendanceFiles);
            }

            
        }
    }
}