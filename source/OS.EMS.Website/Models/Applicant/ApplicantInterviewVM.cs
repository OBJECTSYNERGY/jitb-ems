﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OS.EMS.Web.Models
{
    public class ApplicantInterviewVM
    {
        public Guid Id { get; set; }
        public Guid ApplicantId { get; set; }

        [StringLength(64), MaxLength(64)]
        public string IntervieSubject { get; set; }

        [StringLength(64), MaxLength(64)]
        public string InterviewResult { get; set; }

        [StringLength(256), MaxLength(256)]
        public string Comments { get; set; }

        [StringLength(32), MaxLength(32)]
        public string InterviewerGuideName1 { get; set; }

        [StringLength(32), MaxLength(32)]
        public string InterviewerGuideName2 { get; set; }

        public int IntCounter { get; set; }

        //[DataType(DataType.Password), Required(), StringLength(16, MinimumLength = 4)]
        //[Required()]
        public DateTime? InterviewDate { get; set; }

    }
}