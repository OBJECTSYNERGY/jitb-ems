﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OS.EMS.Web.Models
{
    public class IdentityOrEmpAuthVM
    {
        public IdentityOrEmpAuthVM()
        {
            PopulateDocumentType();
        }

        public Guid? Id { get; set; }
        public int IdentityOrEmpAuthVMID { get; set; }
        /*public string ListType { get; set; }*/
        //public string DocumentType { get; set; }

        public string DocumentTitle { get; set; }
        public string IssuingAuthority { get; set; }
        public string DocumentNumber { get; set; }
        public DateTime ExpirationDate { get; set; }


        //public string DocumentType { get; set; }

        [Display(Name = "Document Type")]
        public int DocumentType { get; set; }
        public List<SelectListItem> ddlDocumentType
        {
            get;
            set;
        }

        public void PopulateDocumentType()
        {
            ddlDocumentType = new List<SelectListItem>();
            //CmbStarchList.Add(new SelectListItem() { Text = "", Value = "0" });
            ddlDocumentType.Add(new SelectListItem() { Text = "List A - Identity & Employment Authorization", Value = "1" });
            ddlDocumentType.Add(new SelectListItem() { Text = "List B - Identity", Value = "2" });
            ddlDocumentType.Add(new SelectListItem() { Text = "List C - Employment Authorization", Value = "3" });
        }

        //[Display(Name = "State")]
        //public int State { get; set; }
        //public List<SelectListItem> ddlState
        //{
        //    get;
        //    set;
        //}

        //public void PopulateStates()
        //{
        //    ddlState = new List<SelectListItem>();
        //    ddlState.AddRange(GenericHelper.GetStateList());
        //}
    }
}