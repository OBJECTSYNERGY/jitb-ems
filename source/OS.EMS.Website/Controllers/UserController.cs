﻿using System;
using System.Web.Mvc;
using OS.EMS.Model;
using OS.EMS.Services;
using OS.EMS.Model.Helper;
using OS.EMS.Web.Models;
using Ninject;
using System.Collections.Generic;
using System.Linq;

namespace OS.EMS.Web.Controllers
{
    public class UserController : BaseController
    {

        [Inject]
        public UserController(ISecurityService securityService, IEmployeeService employeeService)
        {
            SecurityService = securityService;
            EmployeeService = employeeService;
        }

        #region CRUD Operations
        public ActionResult Create()
        {
            var viewModel = new SystemUserVM();
            PopulateRoleForSLI(viewModel);
            PopulateSupervisorForSLI(viewModel);
            return View("CreateOrUpdate", viewModel);

        }

        [HttpPost]
        public ActionResult Create(SystemUserVM viewModel)
        {
            if (viewModel == null) return RedirectToAction("InvalidRequest", "Error");

            var user = viewModel.ToModel();

            if (SecurityService.Create(user))
            {
                ShowMessage(Messages.CreateUserSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult Update(Guid? id)
        {
            if (id == null) return RedirectToAction("NotFound", "Error");

            var user = SecurityService.Get(id.Value);
            var viewModel = user.ToSystemUserVM();
            viewModel.PasswordConfirm = viewModel.Password;

            PopulateRoleForSLI(viewModel);
            PopulateSupervisorForSLI(viewModel);

            return View("CreateOrUpdate", viewModel);
        }

        [HttpPost]
        public ActionResult Update(SystemUserVM viewModel)
        {
            if (viewModel == null || viewModel.Id == null) return RedirectToAction("InvalidRequest", "Error");

            //var user = SecurityService.Get(viewModel.Id.Value);
            var model = viewModel.ToModel();
            if (SecurityService.Update(model))
            {
                ShowMessage(Messages.UpdateUserSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("InvalidRequest", "Error");

        }

        public ActionResult List()
        {
            var users = SecurityService.GetAll();
            return View(users);
        }

        public ActionResult Activate(Guid? id, bool active)
        {
            if (!id.HasValue) return RedirectToAction("InvalidRequest", "Error");

            if (SecurityService.Activate(id.Value, active))
            {
                ShowMessage(active ? Messages.ActivateUserSuccess : Messages.DeactivateUserSuccess, MessageType.Success);
                return RedirectToAction("List");
            }

            return RedirectToAction("List");
        }

        public ActionResult AssociateSystemUser(Guid? id)
        {
            if (!id.HasValue) return RedirectToAction("InvalidRequest", "Error");

            Employee employee = EmployeeService.Get(id.Value);

            //For Mapping matcing Values
            var viewModel = new SystemUserVM();
            Utils.Map(employee, viewModel);
            viewModel.Id = null;
            viewModel.EmployeeId = id.Value;

            PopulateRoleForSLI(viewModel);
            //PopulateSupervisorForSLI(viewModel);

            return View("CreateOrUpdate", viewModel);

        }

        [HttpGet]
        public virtual JsonResult IsEmailExists(string Email, string InitialEmail)
        {
            //Thread.Sleep(2000);//fake delay
            if (Email == InitialEmail)
            {
                return this.Json(true, JsonRequestBehavior.AllowGet);
            }
            return this.Json(!SecurityService.IsEmailExists(Email), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region HelperMethods
        private void PopulateRoleForSLI(SystemUserVM viewModel)
        {
            var roles = SecurityService.GetRoles();
            viewModel.Roles = roles.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = s.Name }).OrderByDescending(x => x.Value);
        }

        private void PopulateSupervisorForSLI(SystemUserVM viewModel)
        {
            var managers = EmployeeService.GetManagers(true);
            viewModel.Managers = managers.Select(s => new SelectListItem { Value = s.Id.ToString(), Text = string.Format("{0} {1} [{2}]", s.LastName, s.FirstName, s.Code) });
        }
        #endregion
    }
}
