using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace OS.EMS.Model
{
    public class Applicant : AuditEntity
    {

        public Applicant()
        {
            //Interviews = new List<ApplicantInterview>();
            //Educations = new List<ApplicantEducation>();
            //ApplicantWorkHistories = new List<ApplicantWorkHistory>();
        }

        [StringLength(32)]
        public string Code { get; set; }

        //EDUCATION
        public virtual ICollection<ApplicantEducation> Educations { get; set; }

        //For EnrolledSchool
        public bool IsSchoolEnrolled { get; set; }
        public string EnrolledName { get; set; }
        public string EnrolledLocation { get; set; }


        ////FOR WORK HISTORY
        //public bool IsWorkedBefore { get; set; }
        public virtual ICollection<ApplicantWorkHistory> ApplicantWorkHistories { get; set; }

        //FOR INTERVIEWS
        public virtual ICollection<ApplicantInterview> Interviews { get; set; }


        //FOR PERSONAL
        [StringLength(32)]
        public string LastName { get; set; }

        [StringLength(32)]
        public string FirstName { get; set; }

        [StringLength(16)]
        public string MiddleInitial { get; set; }

        [StringLength(128)]
        public string Address { get; set; }

        public Guid? CityId { get; set; }
        public virtual City City { get; set; }

        public Guid? StateId { get; set; }
        public virtual State State { get; set; }

        //public Guid CountryId { get; set; }
        //public virtual Country Country { get; set; }

        [StringLength(16)]
        public string ZipCode { get; set; }

        [StringLength(32)]
        public string Phone { get; set; }

        [StringLength(64)]
        public string Email { get; set; }

        public DateTime DateOfBirth { get; set; }
        public bool? IsOverAge { get; set; }

        //Eligibility to work
        public bool? CanProofElegiblity { get; set; }

        //Emergency
        [StringLength(32)]
        public string EmergencyContactName { get; set; }

        [StringLength(32)]
        public string EmergencyPhone { get; set; }

        [StringLength(128)]
        public string EmergencyAddress { get; set; }

        //FOR AVABILITY
        [StringLength(64)]
        public string ContactUsSource { get; set; }

        [StringLength(32)]
        public string PositionsDesired { get; set; }

        public DateTime? DateOfAvailability { get; set; }
        
        public int TotalWeekHours { get; set; }
        //public virtual ICollection<ApplicantAvalability> AvailabilityDetail { get; set; }

        public bool? IsSesonalEmployment { get; set; }
        public DateTime? SesonalEmploymentFrom { get; set; }
        public DateTime? SesonalEmploymentTo { get; set; }

        //Does any thing prevent to report at work
        public bool? IsPreventToWorkDaily { get; set; }

        [StringLength(256)]
        public string PreventToWorkReason { get; set; }

        //FOR MISCELLANEOUS (Previous working detail, Relative WithUs, Crime)
        public bool? IsPreviouslyWorkedWithUs { get; set; }

        [StringLength(32)]
        public string PreviousWorkedStoreCode { get; set; }

        [StringLength(16)]
        public string PreviousWorkedContact { get; set; }

        public bool? IsCompanyRestaurant { get; set; }
        public DateTime? PreviousWorkedFrom { get; set; }
        public DateTime? PreviousWorkedTo { get; set; }

        [StringLength(256)]
        public string PreviousLeavingReason { get; set; }

        [StringLength(128)]
        public string PreviousLocation { get; set; }

        [StringLength(32)]
        public string PreviousSupervisor { get; set; }

        public bool? IsRelativeWorkedWithUs { get; set; }

        [StringLength(32)]
        public string RelativeName { get; set; }

        [StringLength(32)]
        public string RelativeRelation { get; set; }

        [StringLength(128)]
        public string RelativeLocation { get; set; }

        public bool? CanWorkWithoutAccomodation { get; set; }
        public bool? DoesAgreeSafetyRules { get; set; }

        public bool? HasDeniedDrivingLicense { get; set; }

        [StringLength(256)]
        public string DeniedDrivingLicenseReason { get; set; }

        public bool? HasConvictedCrimed { get; set; }
        public bool? IsConvictedPrisoned { get; set; }
        public bool? IsConvictedPrisonedReleased { get; set; }

        [StringLength(256)]
        public string ConvictedPrisonedReason { get; set; }

        [StringLength(64)]
        public string ConvictedPrisonedCharge { get; set; }

        [StringLength(32)]
        public string ConvictedPrisonedPlace { get; set; }

        [StringLength(64)]
        public string ConvictedPrisonedAction { get; set; }

   

    }
}