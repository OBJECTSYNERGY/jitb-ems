﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OS.EMS.Model
{
    public class Store : AuditEntity
    {
        [StringLength(32)]
        public string Code { get; set; }

        [StringLength(32)]
        public string Name { get; set; }

        public Guid? SupervisorId { get; set; }
        public virtual Employee Supervisor { get; set; }

        //public Guid? EmployerId { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }

        [StringLength(64)]
        public string Email { get; set; }

        [StringLength(16)]
        public string Phone { get; set; }

        [StringLength(128)]
        public string Address { get; set; }

        public Guid? CityId { get; set; }
        public virtual City City { get; set; }

        public Guid? StateId { get; set; }
        public virtual State State { get; set; }

        [StringLength(16)]
        public string ZipCode { get; set; }
    }
}


 
